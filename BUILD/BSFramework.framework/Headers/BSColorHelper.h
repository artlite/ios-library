//
//  BSColorHelper.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/28/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Class which provide the helper for the UIColor
 */
@interface BSColorHelper : NSObject

/**
 Method which provide the create color from hex string

 @param hex instance of the {@link NSString}
 @return instance of the {@link UIColor}
 */
+ (UIColor * _Nonnull) create: (NSString * _Nullable) hex;

/**
 Method which provide the create color from hex string
 
 @param hex instance of the {@link NSString}
 @param alpha instance of the {@link CGFloat}
 @return instance of the {@link UIColor}
 */
+ (UIColor * _Nonnull) create: (NSString * _Nullable) hex
                    withAlpha: (CGFloat) alpha;

@end

NS_ASSUME_NONNULL_END
