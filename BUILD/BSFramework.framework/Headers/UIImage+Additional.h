//
//  UIImage+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/27/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Class which provide the additional functionality
 */
@interface UIImage (Additional)

/**
 Method which provide to show preview controller
 */
- (void) showPreview;

@end

NS_ASSUME_NONNULL_END
