//
//  BSLogger.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSLogger : NSObject

/**
 Method which provide the configuring of the {@link BSLogger}

 @param isNeedLogging {@link BOOL} value if the logging is needed
 */
+(void) configure: (BOOL) isNeedLogging;

/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param message {@link NSString} value of the message
 */
+(void) info: (id) owner
  withObject: (NSObject * _Nullable) object;


/**
 Method which provide the show information with parameters

 @param owner instance of the {@link NSObject}
 @param method {@link NSString} value of the method
 @param message {@link NSString} value of the message
 */
+(void) info: (id) owner
  withMethod: (NSString * _Nullable) method
  withObject: (NSObject * _Nullable) object;

/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param message {@link NSObject} value of the message
 */
+(void) error: (id) owner
   withObject: (NSObject * _Nullable) object;


/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param method {@link NSString} value of the method
 @param message {@link NSObject} value of the message
 */
+(void) error: (id) owner
   withMethod: (NSString * _Nullable) method
   withObject: (NSObject * _Nullable) object;

@end

NS_ASSUME_NONNULL_END
