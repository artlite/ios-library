//
//  NSString+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/25/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Categorie which provide the additional functional for the NSString
 */
@interface NSString (Additional)

/**
 Method which provide the trin functionality
 
 @return instance of the {@link NSString}
 */
- (NSString *) trim;

@end

NS_ASSUME_NONNULL_END
