//
//  BSCheckBoxOnly.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/18/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Delegate for the {@link BSCheckBoxЪ
 */
@protocol BSCheckBoxOnlyDelegate <NSObject>

/**
 Method which provide the action when check box was clicked
 
 @param checkBox instance of the {@link BSCheckBox}
 @param isChecked value
 */
- (void) checkBoxOnly: (id) checkBox
              clicked: (BOOL) isChecked;

@end

/**
 Class which provide the check box functionality
 */
@interface BSCheckBoxOnly : BSView

/**
 Instance of the {@link BSCheckBoxDelegate}
 */
@property (nonatomic, weak, nullable) id<BSCheckBoxOnlyDelegate> delegate;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable UIColor * checkBoxColor;

/**
 Text size
 */
@property(nonatomic) IBInspectable CGFloat checkBoxSize;

/**
 Instance of the {@link UIImage}
 */
@property(nonatomic) IBInspectable UIImage * imageChecked;

/**
 Instance of the {@link UIImage}
 */
@property(nonatomic) IBInspectable UIImage * imageUnchecked;

/**
 BOOL value is checked
 */
@property (nonatomic, assign) BOOL isChecked;

@end

NS_ASSUME_NONNULL_END
