//
//  UIViewController+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/26/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Class which provide the additional functional
 */
@interface UIViewController (Additional)

/**
 Method which provide to get of the visible controller

 @return instance of the {@link UIViewController}
 */
+ (UIViewController * _Nullable) getVisibleController;

/**
 Method which provide to get of the visible controller with navigation controller
 
 @return instance of the {@link UIViewController}
 */
+ (UIViewController * _Nullable) getVisibleControllerWithNavigation;

#pragma mark - Pop

/**
 Method which provide the pop to controller
 
 @param class value
 */
- (void) popToController: (Class) class;

/**
 Method which provide the pop to view controller
 
 @param owner class
 @param class value
 */
+ (void) popToController: (UIViewController *) owner
               withClass: (Class) class;

@end

NS_ASSUME_NONNULL_END
