//
//  BSImagePreviewController.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/27/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Controller which provide the image preview
 */
@interface BSImagePreviewController : UIViewController

/**
 Method which provide the show of the image preview

 @param image object
 */
+ (void) show: (UIImage * _Nullable) image;

@end

NS_ASSUME_NONNULL_END
