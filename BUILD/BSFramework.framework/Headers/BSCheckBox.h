//
//  BSCheckBox.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Delegate for the {@link BSCheckBoxЪ
 */
@protocol BSCheckBoxDelegate <NSObject>

/**
 Method which provide the action when check box was clicked

 @param checkBox instance of the {@link BSCheckBox}
 @param isChecked value
 */
- (void) checkBox: (id) checkBox
          clicked: (BOOL) isChecked;

@end

/**
 Class which provide the check box functionality
 */
IB_DESIGNABLE
@interface BSCheckBox : BSView

/**
 Instance of the {@link BSCheckBoxDelegate}
 */
@property (nonatomic, weak, nullable) id<BSCheckBoxDelegate> delegate;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable UIColor * textColor;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable UIColor * checkBoxColor;

/**
 Text size
 */
@property(nonatomic) IBInspectable CGFloat textSize;

/**
 Text size
 */
@property(nonatomic) IBInspectable CGFloat checkBoxSize;

/**
 Instance of the {@link UIImage}
 */
@property(nonatomic) IBInspectable UIImage * imageChecked;

/**
 Instance of the {@link UIImage}
 */
@property(nonatomic) IBInspectable UIImage * imageUnchecked;

/**
 Instance of the {@link NSString}
 */
@property(nonatomic) IBInspectable NSString * checkBoxText;

/**
 BOOL value is checked
 */
@property (nonatomic, assign) BOOL isChecked;

@end

NS_ASSUME_NONNULL_END
