//
//  UIView+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/25/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Enum with dialog type
 */
typedef enum : NSUInteger {
    BSDialogControllerTypeLight,
    BSDialogControllerTypeDark
} BSDialogControllerType;

/**
 Additional functional for the uiview
 */
@interface UIView (Additional)

#pragma mark - Add getsures

/**
 Method which provide the tap functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UITapGestureRecognizer}
 */
- (UITapGestureRecognizer * _Nonnull) addTap: (id) owner
                                withSelector: (SEL) selector;

/**
 Method which provide the long tap functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UILongPressGestureRecognizer}
 */
- (UILongPressGestureRecognizer * _Nonnull) addLongTap: (id) owner
                                          withSelector: (SEL) selector;

/**
 Method which provide the swipe functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UISwipeGestureRecognizer}
 */
- (UISwipeGestureRecognizer * _Nonnull) addSwipe: (id) owner
                                   withDirection: (UISwipeGestureRecognizerDirection) direction
                                    withSelector: (SEL) selector;

/**
 Method which provide the swipe functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UIScreenEdgePanGestureRecognizer}
 */
- (UIScreenEdgePanGestureRecognizer * _Nonnull) addEdgeSwipe: (id) owner
                                                    withEdge:(UIRectEdge) edge
                                                withSelector: (SEL) selector;

#pragma mark - Dialog functional

/**
 Method which provide the action when the dialog controller dismissed
 (SHOULD BE OVERRIDEN IF YOU NEED THE ACTION WHEN DIALOG CONTROLLER CLOSED)
 */
- (void) onDialogControllerDismissed;

/**
 Method which provide the show view as dialog
 
 @param type instance of the {@link BSDialogControllerType}
 */
- (void) showAsDialog: (BSDialogControllerType) type;

/**
 Method which provide the close of the doalog
 */
- (void) closeDialog;

@end

NS_ASSUME_NONNULL_END
