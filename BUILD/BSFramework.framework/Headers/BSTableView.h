//
//  BSTableView.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSView.h>

NS_ASSUME_NONNULL_BEGIN

// MARK: - BSTableViewDelegate

/**
 Delegate which provide the functional for the table view delegate
 */
@protocol BSTableViewDelegate <NSObject>

/**
 Method which provide the action when the event was recieved

 @param tableView instance of the {@link BSTableView}
 @param event {@link NSString} value of the event
 @param object instance of the {@link NSObject}
 */
-(void) tableView: (id) tableView
  didRecieveEvent: (NSString * _Nonnull) event
       withObject: (NSObject * _Nonnull) object;

/**
 Method which provide the refresh functional

 @param tableView instance of the {@link BSTableView}
 */
- (void) tableViewRefresh: (id) tableView;

/**
 Method which provide the refresh functional when almost at bottom

 @param tableView instance of the {@link BSTableView}
 @param size list value
 */
- (void) tableView: (id) tableView
    almostAtBottom: (int) size;

@end

// MARK: - BSTableViewInnerDelegate

/**
 Delegate which provide the functional for the table view delegate
 */
@protocol BSTableViewInnerDelegate <NSObject>

/**
 Method which provide the sending event for the object ID
 
 @param event {@link NSString} value of event
 @param objectId {@link NSString} value of the table ID
 */
-(void) tableViewInnerDelegateSendEvent: (NSString * _Nullable) event
                           withObjectId: (NSString * _Nullable) objectId;

@end

// MARK: - BSTableItemViewDelegate

/**
 Delegate which provide required methods for table view
 */
@protocol BSTableItemViewDelegate<NSObject>
@required

/**
 Method which provide the set object ID from table object
 (YOU NEED TO SAVE OBJECT ID SOMEWERE BECAUSE IT USING FOR REUSE CELLS IN THE TABLE VIEW)
 
 @param objectId {@link NSString} value of the object ID
 */
-(void) tableItemViewUpdateId: (NSString * _Nonnull) objectId;

/**
 Method which provide the update table item view with delegate
 
 @param delegate instance of the {@link BSTableViewInnerDelegate}
 */
-(void) tableItemViewUpdateDelegate: (id<BSTableViewInnerDelegate>) delegate;

/**
 Method which provide the update object inside the view
 (YOU CAN KEEP IT AS WEAK REFERENCE)

 @param object instance of the {@link NSObject}
 */
-(void) tableItemViewUpdateObject: (NSObject * _Nonnull) object;

@end

// MARK: - BSTableObjectDelegate

/**
 Delegate which provide required methods for table object
 */
@protocol BSTableObjectDelegate<NSObject>
@required

/**
 Method which provide the getting of the table object id
 
 @return instance of the {@link NSString}
 */
- (NSString * _Nonnull) tableObjectId;

/**
 Method which provide the getting of the table cell type
 (YOU NEED TO SAVE OBJECT ID SOMEWERE BECAUSE IT USING FOR REUSE CELLS IN THE TABLE VIEW)

 @return instance of the {@link NSString}
 */
- (NSString * _Nonnull) tableCellType;

/**
 Method which provide the getting of the instance of the {@link UIView}
 
 @return instance of the {@link UIView}
 */
- (UIView<BSTableItemViewDelegate> * _Nonnull) tableObjectView;

/**
 Method which provide the configure view with object
 
 @param view instance of the {@link UIView}
 @param cell instance of the {@link UITableViewCell}
 */
- (void) tableObjectConfigure: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                     withCell: (UITableViewCell * _Nonnull) cell;

/**
 Method which provide the functional when item was selected
 
 @param tableView instance of the {@link UITableView}
 @param view instance of the {@link UIView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableObjectDidSelectWithTable:(id)tableView
                            withView: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                           withIndex:(NSIndexPath *)indexPath;

/**
 Method which provide the functional when item was deselected
 
 @param tableView instance of the {@link UITableView}
 @param view instance of the {@link UIView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableObjectDidDeselectWithTable:(id)tableView
                              withView: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                             withIndex:(NSIndexPath *)indexPath;

/**
 Method which provide the getting of the row actions

 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 @param height instance of the {@link CGFloat}
 @return instance of the {@link NSArray}
 */
-(NSArray<UITableViewRowAction *> * _Nullable) tableObjectRowActions: (id) tableView
                                                           withIndex:(NSIndexPath *)indexPath
                                                          withHeight: (CGFloat) height;

/**
 Method which provide the update table item view with delegate
 
 @param delegate instance of the {@link BSTableViewInnerDelegate}
 */
-(void) tableObjectUpdateDelegate: (id<BSTableViewInnerDelegate>) delegate;

@end

// MARK: - BSTableView

/**
 Method which provide the table view functionality
 */
IB_DESIGNABLE
@interface BSTableView : BSView<UITableViewDelegate, UITableViewDataSource, BSTableViewInnerDelegate>

// MARK: - Inspectable

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable BOOL needSwipeDownRefresh;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable BOOL needScrollIndicator;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable BOOL needEmptyView;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable UIColor * separatorColor;

/**
 Instance of the {@link UIColor}
 */
@property(nonatomic) IBInspectable UIColor * refreshColor;

/**
 Instance of the {@link UIScrollViewIndicatorStyle}
 */
@property(nonatomic) IBInspectable UIScrollViewIndicatorStyle indicatorStyle;

/**
 {@link Bool} value of the multiply selection
 */
@property(nonatomic) IBInspectable BOOL selection;

/**
 {@link Bool} value of the multiply selection
 */
@property(nonatomic) IBInspectable BOOL multiplySelection;

/**
 {@link NSString} value of the refresh title
 */
@property(nonatomic, strong) IBInspectable NSString * refreshTitle;

// MARK: - Properties

/**
 Instance of the {@link BSTableViewDelegate}
 */
@property(nonatomic, weak) id<BSTableViewDelegate> delegate;

// MARK: - Clear

/**
 Method which provide the clearing of the objects
 */
-(void) clear;

// MARK: - Set

/**
 Method which provide the set of the view
 
 @param view instance of the {@link UIView}
 */
- (void) setView: (UIView * _Nullable) view;

/**
 Method which provide the adding of the instance of the {@link BSBaseTableObject}
 
 @param object instance of the {@link BSBaseTableObject}
 */
- (void) setObject: (NSObject<BSTableObjectDelegate> * _Nullable) object;

/**
 Method which provide the set of the view
 
 @param views instance of the {@link NSArray}
 */
- (void) setViews: (NSArray<UIView *>* _Nullable) views;

/**
 Method which provide the adding of the instance of the {@link NSArray}
 
 @param objects instance of the {@link NSArray}
 */
- (void) setObjects: (NSArray<NSObject<BSTableObjectDelegate> *> * _Nullable) objects;

// MARK: - Add

/**
 Method which provide the set of the view
 
 @param view instance of the {@link UIView}
 */
- (void) addView: (UIView * _Nullable) view;

/**
 Method which provide the adding of the instance of the {@link BSBaseTableObject}
 
 @param object instance of the {@link BSBaseTableObject}
 */
- (void) addObject: (NSObject<BSTableObjectDelegate> * _Nullable) object;

/**
 Method which provide the set of the view
 
 @param views instance of the {@link NSArray}
 */
- (void) addViews: (NSArray<UIView *>* _Nullable) views;

/**
 Method which provide the adding of the instance of the {@link NSArray}
 
 @param objects instance of the {@link NSArray}
 */
- (void) addObjects: (NSArray<NSObject<BSTableObjectDelegate> *> * _Nullable) objects;

// MARK: - Remove

/**
 Method which provide the removing the object by index
 
 @param object instance of the {@link NSObject<BSTableObjectDelegate>}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObject: (NSObject<BSTableObjectDelegate> * _Nullable) object;

/**
 Method which provide the removing the object by index
 
 @param objects instance of the {@link NSArray}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjects: (NSArray<NSObject<BSTableObjectDelegate> *> * _Nullable) objects;

/**
 Method which provide the removing the object by index
 
 @param index instance of the {@link NSNumber}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndex: (NSNumber * _Nullable) index;

/**
 Method which provide the removing the object by index
 
 @param indexes instance of the {@link NSArray}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndexes: (NSArray<NSNumber *>* _Nullable) indexes;

/**
 Method which provide the removing the object by index path
 
 @param index instance of the {@link NSIndexPath}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndexPath: (NSIndexPath * _Nullable) index;

/**
 Method which provide the removing the object by index path
 
 @param indexes instance of the {@link NSArray}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndexPaths: (NSArray<NSIndexPath *>* _Nullable) indexes;

// MARK: - Get

/**
 Method which provide the getting of the instance of the {@link BSBaseTableObject}
 
 @param index instance of the {@link NSInteger}
 @return instance of the {@link BSBaseTableObject}
 */
- (NSObject<BSTableObjectDelegate> * _Nullable) getObject: (NSNumber * _Nullable) index;

/**
 Method which provide the getting objects
 
 @return instance of the {@link NSArray}
 */
- (NSArray<NSObject<BSTableObjectDelegate> *> * _Nonnull) getObjects;

// MARK: - Notify

/**
 Method which provide the update information
 
 @param needReloadViews {@link BOOL} value if need reloading of the views
 */
- (void) update: (BOOL) needReloadViews;

/**
 Method which provide the set of the indicator style

 @param style instance of the {@link UIScrollViewIndicatorStyle}
 */
- (void) setIndicatorStyle: (UIScrollViewIndicatorStyle) style;

/**
 Method which provide the show refresh
 */
- (void) showRefresh;

/**
 Method which provide the hide refresh
 */
- (void) hideRefresh;

@end

NS_ASSUME_NONNULL_END
