//
//  BSImagePickerComposer.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/25/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Enum for the result
 */
typedef enum : NSUInteger {
    ImagePickerComposerResultDone,
    ImagePickerComposerResultCancel,
    ImagePickerComposerResultNoImage
} ImagePickerComposerResult;

/**
 Delegate for the {@link BSImagePickerComposer}
 */
@protocol BSImagePickerComposerDelegate <NSObject>

/**
 Method which provide the action when the user finish pick the image

 @param composer instance of the {@link BSImagePickerComposer}
 @param type instance of the {@link ImagePickerComposerResult}
 @param image instance of the {@link UIImage}
 */
- (void) imagePickerComposer: (id) composer
            didRecieveResult: (ImagePickerComposerResult) type
                   withImage: (UIImage * _Nullable) image;

/**
 Method which provide the getting controller for the {@link BSImagePickerComposer}

 @param composer instance of the {@link BSImagePickerComposer}
 @return instance of the {@link UIViewController}
 */
- (UIViewController * _Nonnull) imagePickerComposerController: (id) composer;

@end

/**
 Composer which provide the image picker
 */
@interface BSImagePickerComposer : NSObject

/**
 Constructor with delegate
 
 @param delegate instance of the {@link BSImagePickerComposerDelegate}
 @return instance of the {@link BSImagePickerComposer}
 */
- (instancetype)init: (id<BSImagePickerComposerDelegate>) delegate;

/**
 Method which provide the show picker controller for picking image
 
 @param type instance of the {@link UIImagePickerControllerSourceType}
 */
- (void) pick: (UIImagePickerControllerSourceType) type;

/**
 Method which provide the show picker controller for picking image
 
 @param type instance of the {@link UIImagePickerControllerSourceType}
 @param allowEdit is allow edit image
 */
- (void) pick: (UIImagePickerControllerSourceType) type
     withEdit: (BOOL) allowEdit;

@end

NS_ASSUME_NONNULL_END
