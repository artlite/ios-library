//
//  StoryboardManager.h
//  M.A.C.
//
//  Created by Vladimir Yevdokimov on 5/15/15.
//  Copyright (c) 2015 magnet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BSStoryboardHelper : NSObject

/**
 *  Make defined storyboard as application root
 */
+ (void)initiateStoryboard:(NSString *)storyboard;

/**
 *  Get controllers from defines storyboards
 */
+ (id)initialController:(NSString *)storyboard;


/**
 Method which provide the instantiate the controller from storyboard id and controller id

 @param storyboard {@link NSString} value of the storyboard id
 @param controllerId {@link NSString} value of the controller id
 @return instance of the {@link UIViewController}
 */
+ (id)controllerFrom:(NSString *)storyboard withID:(NSString *)controllerId;

@end
