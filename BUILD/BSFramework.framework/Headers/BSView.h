//
//  BSView.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BSFramework/BSExtendedView.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSView : BSExtendedView

/**
 Method which provide to get of the nib name
 
 @return {@link NSString} value of the nib name
 */
-(NSString *) nibName;

/**
 Method which provide the action when view was created
 */
-(void) onCreateView;

@end

NS_ASSUME_NONNULL_END
