//
//  BSImageManager.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/13/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSImageManager : NSObject

/**
 Method which provide the configure {@link BSImageManager} with max width and height

 @param width max width of the image
 @param hight max height of the image
 */
+(void) configureWithWidth: (int) width withHeight: (int) height;

/**
 Method which provide the download image with URL sync

 @param url {@link NSString} value of the url
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) download: (NSString * _Nullable) url;

@end

NS_ASSUME_NONNULL_END
