//
//  BSExtendedView.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 8/14/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 View which provide the extend functionality
 */
IB_DESIGNABLE
@interface BSExtendedView : UIView

#pragma mark - Properties

/**
 Corner radius
 */
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

/**
 Border width
 */
@property (nonatomic, assign) IBInspectable CGFloat borderWidth;

/**
 Border color
 */
@property (nonatomic, strong) IBInspectable UIColor * borderColor;

/**
 Shadow color
 */
@property (nonatomic, strong) IBInspectable UIColor * shadowColor;

/**
 Shadow radius
 */
@property (nonatomic, assign) IBInspectable CGFloat shadowRadius;

@end

NS_ASSUME_NONNULL_END
