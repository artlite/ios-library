//
//  UIStackView+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Extension which provide the additional functional for the stack view
 */
@interface UIStackView (Additional)

#pragma mark - Show / Hide

/**
 Method which provide the hide all arranged views
 */
- (void) hideAllArrangedViews;

/**
 Method which provide the show all arranged views
 */
- (void) showAllArrangedViews;

#pragma mark - Remove

/**
 Method which provide the remove all arranged views
 */
- (void) removeAllArrangedViews;

/**
 Method which provide the remove views by type
 
 @param classes array of the classes type
 */
- (void) removeArrangedViews: (NSArray<Class>* _Nullable) classes;

@end

NS_ASSUME_NONNULL_END
