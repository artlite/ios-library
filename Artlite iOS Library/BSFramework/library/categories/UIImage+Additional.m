//
//  UIImage+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/27/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/UIImage+Additional.h>

/**
 Class which provide the additional functionality
 */
@implementation UIImage (Additional)

/**
 Method which provide to show preview controller
 */
- (void) showPreview {
    [BSImagePreviewController show:self];
}

@end
