//
//  UITableViewRowAction+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/8/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Extension which provide the additional action for the {@link UITableViewRowAction}
 */
@interface UITableViewRowAction (Additional)

/**
 Method which provide to create the {@link UITableViewRowAction} from image and height

 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 @param height {@link CGFloat} value of the height
 @param handler instance of the handler
 @return instance of the {@link UITableViewRowAction}
 */
+ (UITableViewRowAction * _Nonnull) actionFromImage: (UIImage * _Nonnull) image
                                          withColor: (UIColor *) color
                                          withHeight: (CGFloat) height
                                         withHandler:(void (^)(UITableViewRowAction *action, NSIndexPath *indexPath))handler;

@end

NS_ASSUME_NONNULL_END
