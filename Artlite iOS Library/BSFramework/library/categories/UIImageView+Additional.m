//
//  UIImageView+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/27/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/UIImageView+Additional.h>

/**
 Additional functional for the UIImageView
 */
@implementation UIImageView (Additional)

#pragma mark - Preview

/**
 Getter for need preview

 @return value
 */
-(BOOL)clickPreview {
    return self.userInteractionEnabled;
}

/**
 Getter for need preview
 
 @return value
 */
-(BOOL)longClickPreview {
    return self.userInteractionEnabled;
}

/**
 Setter for property

 @param needPreview value
 */
-(void)setClickPreview:(BOOL)clickPreview {
#if !TARGET_INTERFACE_BUILDER
    if (clickPreview == YES) {
        self.userInteractionEnabled = YES;
        [self addTap:self withSelector:@selector(showPreview)];
    }
#endif
}

/**
 Setter for property
 
 @param needPreview value
 */
-(void)setLongClickPreview:(BOOL)longClickPreview {
#if !TARGET_INTERFACE_BUILDER
    if (longClickPreview == YES) {
        self.userInteractionEnabled = YES;
        [self addLongTap:self withSelector:@selector(showPreview)];
    }
#endif
}

/**
 Method which provide to show preview
 */
- (void) showPreview {
    [BSImagePreviewController show:self.image];
}

@end
