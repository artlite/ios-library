//
//  NSString+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/25/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/NSString+Additional.h>

/**
 Categorie which provide the additional functional for the NSString
 */
@implementation NSString (Additional)

/**
 Method which provide the trin functionality

 @return instance of the {@link NSString}
 */
- (NSString *) trim {
    return [self stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
