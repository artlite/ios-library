//
//  UIStackView+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/UIStackView+Additional.h>

/**
 Extension which provide the additional functional for the stack view
 */
@implementation UIStackView (Additional)

/**
 Method which provide the hide all arranged views
 */
- (void) hideAllArrangedViews {
    NSArray<UIView *>* views = self.arrangedSubviews;
    if ((views != nil) && (views.count > 0)) {
        for (UIView * view in views) {
            view.hidden = YES;
        }
    }
}

/**
 Method which provide the show all arranged views
 */
- (void) showAllArrangedViews {
    NSArray<UIView *>* views = self.arrangedSubviews;
    if ((views != nil) && (views.count > 0)) {
        for (UIView * view in views) {
            view.hidden = NO;
        }
    }
}

#pragma mark - Show / Hide

/**
 Method which provide the remove all arranged views
 */
- (void) removeAllArrangedViews {
    NSArray<UIView *>* views = self.arrangedSubviews;
    if ((views != nil) && (views.count > 0)) {
        for (UIView * view in views) {
            [view removeFromSuperview];
        }
    }
}


/**
 Method which provide the remove views by type

 @param classes array of the classes type
 */
- (void) removeArrangedViews: (NSArray<Class>* _Nullable) classes {
    if ((classes == nil) || (classes.count <= 0)) {
        [self removeAllArrangedViews];
        return;
    }
    NSArray<UIView *>* views = self.arrangedSubviews;
    if ((views != nil) && (views.count > 0)) {
        for (UIView * view in views) {
            Class aClass = [view classForCoder];
            if ([classes containsObject:aClass]) {
                [view removeFromSuperview];
            }
        }
    }
}

@end
