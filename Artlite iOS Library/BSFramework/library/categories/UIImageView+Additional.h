//
//  UIImageView+Additional.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/27/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Additional functional for the UIImageView
 */
IB_DESIGNABLE
@interface UIImageView (Additional)

/**
 Boolean value if it need preview
 */
@property (nonatomic) IBInspectable BOOL clickPreview;

/**
 Boolean value if it need preview
 */
@property (nonatomic) IBInspectable BOOL longClickPreview;

@end

NS_ASSUME_NONNULL_END
