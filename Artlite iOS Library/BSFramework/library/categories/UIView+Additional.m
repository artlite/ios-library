//
//  UIView+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/25/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/UIView+Additional.h>

/**
 Additional functional for the uiview
 */
@implementation UIView (Additional)

#pragma mark - Add getsures


/**
 Method which provide the tap functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UITapGestureRecognizer}
 */
- (UITapGestureRecognizer * _Nonnull) addTap: (id) owner
                                withSelector: (SEL) selector {
    return [BSGestureHelper addTap:owner
                          withView:self
                      withSelector:selector];
}

/**
 Method which provide the long tap functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UILongPressGestureRecognizer}
 */
- (UILongPressGestureRecognizer * _Nonnull) addLongTap: (id) owner
                                          withSelector: (SEL) selector {
    return [BSGestureHelper addLongTap:owner
                              withView:self
                          withSelector:selector];
}

/**
 Method which provide the swipe functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UISwipeGestureRecognizer}
 */
- (UISwipeGestureRecognizer * _Nonnull) addSwipe: (id) owner
                                   withDirection: (UISwipeGestureRecognizerDirection) direction
                                    withSelector: (SEL) selector {
    return [BSGestureHelper addSwipe:owner
                            withView:self
                       withDirection:direction
                        withSelector:selector];
}

/**
 Method which provide the swipe functional
 
 @param owner object
 @param selector for execution
 @return instance of the {@link UIScreenEdgePanGestureRecognizer}
 */
- (UIScreenEdgePanGestureRecognizer * _Nonnull) addEdgeSwipe: (id) owner
                                                    withEdge:(UIRectEdge) edge
                                                withSelector: (SEL) selector {
    return [BSGestureHelper addEdgeSwipe:owner
                                withView:self
                                withEdge:edge
                            withSelector:selector];
}

/**
 Method which provide the action when the dialog controller dismissed
 (SHOULD BE OVERRIDEN IF YOU NEED THE ACTION WHEN DIALOG CONTROLLER CLOSED)
 */
- (void) onDialogControllerDismissed {
    // MARK
}

/**
 Method which provide the show view as dialog

 @param type instance of the {@link BSDialogControllerType}
 */
- (void) showAsDialog: (BSDialogControllerType) type {
    [BSDialogController show:@[self] withType:type];
}

/**
 Method which provide the close of the doalog
 */
- (void) closeDialog {
    [BSDialogController close];
}


@end
