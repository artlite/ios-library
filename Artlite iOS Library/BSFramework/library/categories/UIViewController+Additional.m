//
//  UIViewController+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/26/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/UIViewController+Additional.h>

@implementation UIViewController (Additional)

/**
 Method which provide to get of the visible controller
 
 @return instance of the {@link UIViewController}
 */
+ (UIViewController * _Nullable) getVisibleController {
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    if (window != nil) {
        return window.rootViewController;
    }
    return nil;
}

/**
 Method which provide to get of the visible controller with navigation controller
 
 @return instance of the {@link UIViewController}
 */
+ (UIViewController * _Nullable) getVisibleControllerWithNavigation {
    UIViewController * controller = [UIViewController getVisibleController];
    if ((controller != nil) && (controller.navigationController != nil)) {
        return controller;
    }
    return nil;
}

#pragma mark - Pop

/**
 Method which provide the pop to controller

 @param class value
 */
- (void) popToController: (Class) class {
    [UIViewController popToController:self withClass:class];
}

/**
 Method which provide the pop to view controller

 @param owner class
 @param class value
 */
+ (void) popToController: (UIViewController *) owner
               withClass: (Class) class {
    if (owner == nil) return;
    if (owner.navigationController == nil) return;
    UINavigationController * navigationController = owner.navigationController;
    if (navigationController != nil) {
        UIViewController * controller = nil;
        for (UIViewController * item in navigationController.viewControllers) {
            if ([item isKindOfClass:class] == YES) {
                controller = item;
                break;
            }
        }
        if (controller != nil) {
            [navigationController popToViewController:controller animated:YES];
        }
    }
}

@end
