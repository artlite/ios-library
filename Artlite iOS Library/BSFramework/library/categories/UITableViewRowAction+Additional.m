//
//  UITableViewRowAction+Additional.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/8/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/UITableViewRowAction+Additional.h>

/**
 Extension which provide the additional action for the {@link UITableViewRowAction}
 */
@implementation UITableViewRowAction (Additional)

/**
 Instance of the {@link CGFloat}
 */
static CGFloat const K_WIDTH = 45;

/**
 Method which provide to create the {@link UITableViewRowAction} from image and height
 
 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 @param height {@link CGFloat} value of the height
 @param handler instance of the handler
 @return instance of the {@link UITableViewRowAction}
 */
+ (UITableViewRowAction * _Nonnull) actionFromImage: (UIImage * _Nonnull) image
                                          withColor: (UIColor *) color
                                         withHeight: (CGFloat) height
                                        withHandler:(void (^)(UITableViewRowAction *action, NSIndexPath *indexPath))handler {
    UITableViewRowAction * action = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault
                                                                       title:@"" handler:handler];
    UIImage * resizedImage = [BSImageHelper resize:image withWidth:K_WIDTH andHeight:K_WIDTH];
    UIImage * backgroundImage = [UITableViewRowAction image:resizedImage withColor:color withHeight:height];
    action.backgroundColor = [UIColor colorWithPatternImage:backgroundImage];
    return action;
}

/**
 Method which provide the convert image to background

 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 @param height {@link CGFloat} value of the height
 @return instance of the {@link UIImage}
 */
+ (UIImage*)image:(UIImage *) image
        withColor: (UIColor *) color
       withHeight:(CGFloat)height{
    CGRect frame = CGRectMake(0, 0, 1000, height);
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(1000, height), NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, color.CGColor);
    CGContextFillRect(context, frame);
    [image drawInRect:CGRectMake(15, height/2.0 - (K_WIDTH / 2.0), K_WIDTH, K_WIDTH)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
