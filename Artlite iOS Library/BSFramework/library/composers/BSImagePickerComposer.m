//
//  BSImagePickerComposer.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/25/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSImagePickerComposer.h>

/**
 Interface
 */
@interface BSImagePickerComposer()<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (nonatomic, weak) id<BSImagePickerComposerDelegate> delegate;
@property (nonatomic, strong, nonnull) UIImagePickerController * pickerController;
@end

/**
 Composer which provide the image picker
 */
@implementation BSImagePickerComposer

/**
 Constructor with delegate
 
 @param delegate instance of the {@link BSImagePickerComposerDelegate}
 @return instance of the {@link BSImagePickerComposer}
 */
- (instancetype)init: (id<BSImagePickerComposerDelegate>) delegate
{
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.pickerController = [[UIImagePickerController alloc]init];
        self.pickerController.delegate = self;
    }
    return self;
}

#pragma mark - UIImagePickerControllerDelegate

/**
 Method which provide the getting of the image picker
 
 @param picker instance of the {@link UIImagePickerController}
 @param info instance of the {@link NSDictionary}
 */
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerControllerInfoKey,id> *)info {
    if (self.delegate != nil) {
        UIImage * image = info[(picker.allowsEditing == YES) ? UIImagePickerControllerEditedImage : UIImagePickerControllerOriginalImage];
        if (image != nil) {
            [self.delegate imagePickerComposer:self
                              didRecieveResult:ImagePickerComposerResultDone
                                     withImage:image];
        } else {
            [self.delegate imagePickerComposer:self
                              didRecieveResult:ImagePickerComposerResultNoImage
                                     withImage:nil];
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

/**
 Method which provide the cancel of the image picker controller

 @param picker instance of the {@link UIImagePickerController}
 */
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    if (self.delegate != nil) {
        [self.delegate imagePickerComposer:self
                          didRecieveResult:ImagePickerComposerResultCancel
                                 withImage:nil];
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

/**
 Method which provide the show picker controller for picking image
 
 @param type instance of the {@link UIImagePickerControllerSourceType}
 */
- (void) pick: (UIImagePickerControllerSourceType) type {
    [self pick:type withEdit:NO];
}

/**
 Method which provide the show picker controller for picking image

 @param type instance of the {@link UIImagePickerControllerSourceType}
 @param allowEdit is allow edit image
 */
- (void) pick: (UIImagePickerControllerSourceType) type
     withEdit: (BOOL) allowEdit {
    self.pickerController.allowsEditing = allowEdit;
    self.pickerController.sourceType = type;
    if (self.delegate != nil) {
        UIViewController * controller = [self.delegate imagePickerComposerController:self];
        if (controller != nil) {
            [controller presentViewController:self.pickerController animated:YES completion:nil];
        }
    }
}

@end
