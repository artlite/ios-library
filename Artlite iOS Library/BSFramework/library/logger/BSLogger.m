//
//  BSLogger.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSLogger.h>

@interface BSLogger()
@property (nonatomic, assign) BOOL isNeedLogging;
@end

@implementation BSLogger

// MARK: - Variables
static BSLogger * instance;
static NSString * const K_ERROR_TAG = @"!!! [ERROR]";
static NSString * const K_INFO_TAG = @"    [INFO]";
static NSString * const K_MESSAGE_FORMAT = @"\n\t%@ %@ -> %@ => %@";

/**
 Default constructor

 @return instance of the {@link BSLogger}
 */
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isNeedLogging = YES;
    }
    return self;
}

/**
 Method which provide the getting of the instance of the {@link BSLogger}
 
 @return instance of the {@link BSLogger}
 */
+(BSLogger *)shared {
    if (instance == nil) {
        instance = [[BSLogger alloc] init];
    }
    return instance;
}

/**
 Method which provide the configuring of the {@link BSLogger}
 
 @param isNeedLogging {@link BOOL} value if the logging is needed
 */
+(void)configure:(BOOL)isNeedLogging {
    [BSLogger shared].isNeedLogging = isNeedLogging;
}

/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param message {@link NSString} value of the message
 */
+(void) info: (id) owner
  withObject: (NSObject * _Nullable) object {
    [self info: owner withMethod: nil withObject: object];
}


/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param method {@link NSString} value of the method
 @param message {@link NSString} value of the message
 */
+(void) info: (id) owner
  withMethod: (NSString * _Nullable) method
  withObject: (NSObject * _Nullable) object {
    if ([BSLogger shared].isNeedLogging == YES) {
        NSLog(K_MESSAGE_FORMAT, K_INFO_TAG, [owner class], method, object);
    }
}

/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param message {@link NSObject} value of the message
 */
+(void) error: (id) owner
   withObject: (NSObject * _Nullable) object {
    [self error: owner withMethod: nil withObject: object];
}


/**
 Method which provide the show information with parameters
 
 @param owner instance of the {@link NSObject}
 @param method {@link NSString} value of the method
 @param message {@link NSObject} value of the message
 */
+(void) error: (id) owner
   withMethod: (NSString * _Nullable) method
   withObject: (NSObject * _Nullable) object {
    if ([BSLogger shared].isNeedLogging == YES) {
        NSLog(K_MESSAGE_FORMAT, K_ERROR_TAG, [owner class], method, object);
    }
}

@end
