//
//  BSImageManager.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/13/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSImageManager.h>

@interface BSImageManager()
@property(nonatomic, assign) int maxWidth;
@property(nonatomic, assign) int maxHeight;
@end

@implementation BSImageManager

// MARK: - Variables

/**
 Instance of the {@link BSImageManager}
 */
static BSImageManager * instance;

/**
 Instance of the {@link NSString}
 */
static NSString * const K_TYPE = @"bsimagecache";

/**
 Method which provide the getting of the instance of the {@link BSImageManager}

 @return instance of the {@link BSImageManager}
 */
+(BSImageManager * _Nonnull) shared {
    if (instance == nil) {
        instance = [[BSImageManager alloc] init];
    }
    return instance;
}

/**
 Default constructor

 @return instance of the {@link BSImageManager}
 */
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.maxWidth = 100;
        self.maxHeight = 100;
    }
    return self;
}

/**
 Method which provide the configure {@link BSImageManager} with max width and height
 
 @param width max width of the image
 @param hight max height of the image
 */
+(void) configureWithWidth: (int) width withHeight: (int) height {
    [BSImageManager shared].maxWidth = width;
    [BSImageManager shared].maxHeight = height;
}

/**
 Method which provide the download image with URL sync
 
 @param url {@link NSString} value of the url
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) download: (NSString * _Nullable) url {
    if (url == nil) {
        return nil;
    }
    NSString * const name = [BSImageManager encodeStringTo64: url];
    UIImage * const cachedImage = [BSImageManager read: name];
    if (cachedImage != nil) {
        return cachedImage;
    }
    NSURL * const urlObject = [[NSURL alloc] initWithString: url];
    if (urlObject != nil) {
        NSMutableURLRequest * const request = [[NSMutableURLRequest alloc] initWithURL: urlObject];
        NSURLResponse * response = nil;
        NSData * urlData = nil;
        NSError * error = nil;
        @try {
            urlData = [NSURLConnection sendSynchronousRequest:
                       request returningResponse:
                       &response error: &error];
            return [BSImageManager save: urlData withName: name];
        } @catch (NSException *exception) {
            [BSLogger error: [BSImageManager shared] withObject: exception];
        }
    }
    return nil;
}

/**
 Method which provide the getting of the file path

 @param name {@link NSString} value of the name
 @return {@link NSString} value of the path
 */
+(NSString * _Nonnull) getPath: (NSString * _Nonnull) name {
    NSString * const directory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSString * const filePath = [directory stringByAppendingPathComponent: name];
    return filePath;
}

/**
 Method which provide the save of the {@link NSData} to the file

 @param data instance of the {@link NSData}
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nonnull) save: (NSData * _Nonnull) data withName: (NSString * _Nonnull) name {
    NSString * const filePath = [BSImageManager getPath: name];
    UIImage * const image = [BSImageManager resize: data];
    NSData * const resizedData = UIImageJPEGRepresentation(image, 1.0);
    @try {
        BOOL const saveResult = [resizedData writeToFile:filePath atomically: YES];
        if (saveResult == YES) {
            [BSLogger info: [BSImageManager shared] withObject: @"Saved successfully"];
        } else {
            [BSLogger info: [BSImageManager shared] withObject: @"Saved error"];
        }
    } @catch (NSException *exception) {
        [BSLogger error: [BSImageManager shared] withObject: exception];
    }
    return image;
}

/**
 Method which provide the readin instance of the {@link UIImage} from file

 @param name {@link NSString} value of the name
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) read: (NSString * _Nonnull) name {
    NSString * const filePath = [BSImageManager getPath: name];
    NSFileManager * const fileManager = [NSFileManager defaultManager];
    BOOL const success = [fileManager fileExistsAtPath: filePath];
    UIImage * image = nil;
    if(!success){
        return nil;
    }else{
        image = [[UIImage alloc] initWithContentsOfFile: filePath];
    }
    return image;
}

/**
 Method which provide the scaling of the instance of the {@link UIImage}

 @param data instance of the {@link NSData}
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nonnull) resize: (NSData * _Nonnull) data {
    int const width = [BSImageManager shared].maxWidth;
    int const height = [BSImageManager shared].maxHeight;
    CGSize newSize = CGSizeMake(width, height);
    CGRect scaledImageRect = CGRectZero;
    UIImage * const image = [[UIImage alloc] initWithData: data];
    CGFloat aspectWidth = newSize.width/image.size.width;
    CGFloat aspectHeight = newSize.height/image.size.height;
    CGFloat aspectRatio = MIN(aspectWidth, aspectHeight);
    scaledImageRect.size.width = image.size.width * aspectRatio;
    scaledImageRect.size.height = image.size.height * aspectRatio;
    scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0f;
    scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0f;
    UIGraphicsBeginImageContextWithOptions( newSize, NO, 0 );
    [image drawInRect:scaledImageRect];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}


/**
 Method which provide the convert the instance of the {@link NSString}

 @param fromString instance of the {@link NSString}
 @return instance of the {@link NSString}
 */
+(NSString*)encodeStringTo64:(NSString*)fromString
{
    int const width = [BSImageManager shared].maxWidth;
    int const height = [BSImageManager shared].maxHeight;
    CC_MD5_CTX md5;
    CC_MD5_Init (&md5);
    CC_MD5_Update (&md5, [fromString UTF8String], (CC_LONG) [fromString length]);
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5_Final (digest, &md5);
    NSString *s = [NSString stringWithFormat:
                   @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                   digest[0],  digest[1],
                   digest[2],  digest[3],
                   digest[4],  digest[5],
                   digest[6],  digest[7],
                   digest[8],  digest[9],
                   digest[10], digest[11],
                   digest[12], digest[13],
                   digest[14], digest[15]];
    return [[NSString alloc] initWithFormat:@"%@_size%dx%d.%@", s, width, height, K_TYPE];
}

@end
