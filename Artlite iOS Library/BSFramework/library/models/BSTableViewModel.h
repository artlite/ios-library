//
//  BSTableViewModel.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/7/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Table model which provide to add of the view inside the table
 */
@interface BSTableViewModel : NSObject<BSTableObjectDelegate>

/**
 Constructor which provide to create the {@link BSTableViewModel} with parameters
 
 @param view instance of the {@link UIView}
 @return instance of the {@link BSTableViewModel}
 */
- (instancetype)init: (UIView * _Nonnull) view;

@end

NS_ASSUME_NONNULL_END
