//
//  BSTableViewModel.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/7/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSTableViewModel.h>

/**
 Interface for the {@link BSTableViewModel}
 */
@interface BSTableViewModel()
@property(nonatomic, strong, nonnull) NSString * objectId;
@property(nonatomic, strong, nonnull) NSString * type;
@property(nonatomic, strong, nonnull) UIView * innerView;
@property(nonatomic, weak, nullable) id<BSTableViewInnerDelegate> delegate;
@end

/**
 Table model which provide to add of the view inside the table
 */
@implementation BSTableViewModel

/**
 Constructor which provide to create the {@link BSTableViewModel} with parameters

 @param view instance of the {@link UIView}
 @return instance of the {@link BSTableViewModel}
 */
- (instancetype)init: (UIView * _Nonnull) view
{
    self = [super init];
    if (self) {
        self.innerView = view;
    }
    return self;
}

/**
 Method which provide the getting of the table object id
 
 @return instance of the {@link NSString}
 */
- (NSString * _Nonnull) tableObjectId {
    if (self.objectId == nil) {
        self.objectId = [[NSUUID UUID] UUIDString];
    }
    return self.objectId;
}

/**
 Method which provide the getting of the table cell type
 (YOU NEED TO SAVE OBJECT ID SOMEWERE BECAUSE IT USING FOR REUSE CELLS IN THE TABLE VIEW)
 
 @return instance of the {@link NSString}
 */
- (NSString * _Nonnull) tableCellType {
    if (self.type == nil) {
        self.type = [[NSUUID UUID] UUIDString];
    }
    return self.type;
}

/**
 Method which provide the getting of the instance of the {@link UIView}
 
 @return instance of the {@link UIView}
 */
- (UIView *)tableObjectView {
    return self.innerView;
}

/**
 Method which provide the configure view with object
 
 @param view instance of the {@link UIView}
 @param cell instance of the {@link UITableViewCell}
 */
-(void)tableObjectConfigure:(UIView *)view
                   withCell:(UITableViewCell *)cell {
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

/**
 Method which provide the functional when item was selected
 
 @param tableView instance of the {@link UITableView}
 @param view instance of the {@link UIView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableObjectDidSelectWithTable:(id)tableView
                            withView: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                           withIndex:(NSIndexPath *)indexPath {
    
}

/**
 Method which provide the functional when item was deselected
 
 @param tableView instance of the {@link UITableView}
 @param view instance of the {@link UIView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableObjectDidDeselectWithTable:(id)tableView
                              withView: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                             withIndex:(NSIndexPath *)indexPath {
}

/**
 Method which provide the getting of the row actions
 
 @return instance of the {@link NSArray}
 */
-(NSArray<UITableViewRowAction *> * _Nullable) tableObjectRowActions: (id) tableView
                                                           withIndex:(NSIndexPath *)indexPath
                                                          withHeight:(CGFloat)height {
    return nil;
}

/**
 Method which provide the update table item view with delegate
 
 @param delegate instance of the {@link BSTableViewInnerDelegate}
 */
-(void)tableObjectUpdateDelegate:(id<BSTableViewInnerDelegate>)delegate {
    self.delegate = delegate;
}

@end
