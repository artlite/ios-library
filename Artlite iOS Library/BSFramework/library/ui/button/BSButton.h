//
//  BSButton.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BSButton : UIButton

/**
 Method which provide the setting title for all states

 @param title {@link NSString} value of the title
 */
-(void) setTitle: (NSString * _Nullable) title;

@end

NS_ASSUME_NONNULL_END
