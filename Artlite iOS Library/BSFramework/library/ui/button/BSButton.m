//
//  BSButton.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSButton.h>

@implementation BSButton

/**
 Method which provide the setting title for all states
 
 @param title {@link NSString} value of the title
 */
-(void) setTitle: (NSString * _Nullable) title {
#if !TARGET_INTERFACE_BUILDER
    [self setTitle:title forState: UIControlStateNormal];
    [self setTitle:title forState: UIControlStateHighlighted];
    [self setTitle:title forState: UIControlStateSelected];
    [self setTitle:title forState: UIControlStateFocused];
    [self setTitle:title forState: UIControlStateApplication];
    [self setTitle:title forState: UIControlStateReserved];
#endif
}


@end
