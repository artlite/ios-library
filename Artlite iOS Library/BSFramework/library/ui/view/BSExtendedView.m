//
//  BSExtendedView.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 8/14/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSExtendedView.h>

/**
 View which provide the extend functionality
 */
@implementation BSExtendedView

#pragma mark - Corner radius

/**
 Get corner radius
 
 @return radius value
 */
-(CGFloat)cornerRadius {
    return self.layer.cornerRadius;
}

/**
 Method which provide the corner radius
 
 @param cornerRadius value
 */
-(void)setCornerRadius:(CGFloat)cornerRadius {
#if !TARGET_INTERFACE_BUILDER
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds = YES;
#endif
}

#pragma mark - Border Width

/**
 Method which provide the getting of the borded width
 
 @return border width value
 */
-(CGFloat)borderWidth {
    return self.layer.borderWidth;
}

/**
 Method which provide the setting of the border width
 
 @param borderWidth vakue
 */
-(void)setBorderWidth:(CGFloat)borderWidth {
#if !TARGET_INTERFACE_BUILDER
    self.layer.borderWidth = borderWidth;
#endif
}

#pragma mark - Border color

/**
 Method which provide the getting of the borded color
 
 @return border color
 */
-(UIColor *)borderColor {
#if !TARGET_INTERFACE_BUILDER
    CGColorRef color = self.layer.borderColor;
    if (color == nil) {
        color = UIColor.clearColor.CGColor;
    }
    return [UIColor colorWithCGColor:color];
#endif
}

/**
 Method which provide the set border color
 
 @param borderColor value
 */
-(void)setBorderColor:(UIColor *)borderColor {
#if !TARGET_INTERFACE_BUILDER
    if (borderColor != nil) {
        self.layer.borderColor = borderColor.CGColor;
    }
#endif
}

#pragma mark - Shadow color

/**
 Method which provide to get of the shadow color
 
 @return instance of the shadow color
 */
-(UIColor *)shadowColor {
    return [[UIColor alloc] initWithCGColor:self.layer.shadowColor];
}

/**
 Method which provide to set of the shadow color
 
 @param shadowColor value
 */
-(void)setShadowColor:(UIColor *)shadowColor {
#if !TARGET_INTERFACE_BUILDER
    if (shadowColor != nil) {
        self.layer.shadowColor = shadowColor.CGColor;
    }
#endif
}

#pragma mark - Shadow radius

/**
 Method which provide to get of the shadow radius
 
 @return shadow radius
 */
-(CGFloat)shadowRadius {
    return self.layer.shadowRadius;
}

/**
 Method which provide the setting of the shadow radius
 
 @param shadowRadius value
 */
-(void)setShadowRadius:(CGFloat)shadowRadius {
#if !TARGET_INTERFACE_BUILDER
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf != nil) {
            weakSelf.layer.masksToBounds = NO;
            weakSelf.layer.shadowOpacity = 1.0f;
            weakSelf.layer.shadowOffset = CGSizeZero;
            weakSelf.layer.shadowRadius = shadowRadius;
            weakSelf.layer.shadowPath = [UIBezierPath bezierPathWithRoundedRect:weakSelf.bounds cornerRadius:weakSelf.layer.cornerRadius].CGPath;
        }
    });
#endif
}

@end
