//
//  BSSpinningImageView.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
/**
 Image view which provide the spinning
 */
IB_DESIGNABLE
@interface BSSpinningImageView : UIImageView

/**
 Instance of the {@link BOOL}
 */
@property(nonatomic) IBInspectable BOOL autostart;

/**
 Instance of the {@link CGFloat}
 */
@property(nonatomic) IBInspectable CGFloat animationInterval;

/**
 Method which provide the stop spinning
 */
- (void) stopSpinning;

/**
 Method which provide the start spinning
 */
- (void) startSpinning;

@end

NS_ASSUME_NONNULL_END
