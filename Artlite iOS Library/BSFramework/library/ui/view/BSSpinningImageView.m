//
//  BSSpinningImageView.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSSpinningImageView.h>

/**
 Interface for the {@link BSSpinningImageView}
 */
@interface BSSpinningImageView()
@property(nonatomic, assign) BOOL animate;
@property(nonatomic, assign) BOOL animationCompleting;
@property(nonatomic, assign) BOOL animationPending;
@end

@implementation BSSpinningImageView

/**
 Method which provide the action when the view is created
 */
-(void)awakeFromNib {
    [super awakeFromNib];
    self.animate = NO;
    self.animationCompleting = NO;
    self.animationPending = NO;
    if (self.animationInterval < 0) {
        self.animationInterval = 5.0f;
    }
    if (self.autostart == YES) {
        [self startSpinning];
    }
}

/**
 Method which provide the spin view with options
 
 @param options instance of the {@link UIViewAnimationOptions}
 */
- (void) spinLogoWithOptions: (UIViewAnimationOptions) options {
    NSTimeInterval fullSpinInterval = self.animationInterval;
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration: fullSpinInterval / 4.0f
                          delay: 0.0f
                        options: options
                     animations: ^{
                         if (weakSelf != nil) {
                             weakSelf.transform = CGAffineTransformRotate(weakSelf.transform, M_PI / 2.0f);
                         }
                     }
                     completion:^(BOOL finished) {
                         if (weakSelf != nil) {
                             // if flag still set, keep spinning with constant speed
                             if (weakSelf.animate) {
                                 [weakSelf spinLogoWithOptions: UIViewAnimationOptionCurveLinear];
                             } else if (weakSelf.animationPending) {
                                 // another spin has been requested, so start it right back up!
                                 weakSelf.animationPending = NO;
                                 weakSelf.animate = YES;
                                 [weakSelf spinLogoWithOptions: UIViewAnimationOptionBeginFromCurrentState];
                             } else if ((options & UIViewAnimationOptionCurveEaseOut) == 0) {
                                 // one last spin, with deceleration
                                 [weakSelf spinLogoWithOptions: UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionCurveEaseOut];
                             } else {
                                 weakSelf.animationCompleting = NO;
                             }
                         }
                     }];
}

/**
 Method which provide the stop spinning
 */
- (void) stopSpinning {
    self.animate = NO;
    self.animationCompleting = YES;
}

/**
 Method which provide the start spinning
 */
- (void) startSpinning {
    if (!self.animate) {
        if (self.animationCompleting) {
            self.animationPending = YES;
        } else {
            self.animate = YES;
            [self spinLogoWithOptions: UIViewAnimationOptionCurveEaseIn];
        }
    }
}

/**
 Method which provide the preparing for the interface builder
 */
-(void)prepareForInterfaceBuilder {
    
}

@end
