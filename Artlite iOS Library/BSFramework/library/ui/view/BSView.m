//
//  BSView.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSView.h>

/**
 Class which provide the init of the custom view
 */
@implementation BSView

/**
 Method which provide the init view with coder

 @param aDecoder instance of the {@link NSCoder}
 @return instance of the {@link BSView}
 */
-(instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInit];
    }
    return self;
}


/**
 Method which provide the init view with frame

 @param frame instance of the {@link CGRect}
 @return instance of the {@link BSView}
 */
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self customInit];
    }
    return self;
}

/**
 Method which provide the custom init of the {@link BSView}
 */
-(void) customInit {
    NSBundle * bundle = [NSBundle bundleForClass:[self class]];
    UIView * view = [[bundle loadNibNamed:[self nibName] owner:self options:nil] firstObject];
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    view.translatesAutoresizingMaskIntoConstraints = NO;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    view.autoresizesSubviews = YES;
    self.autoresizesSubviews = YES;
    NSLayoutConstraint * _Nonnull leading = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeLeft relatedBy: NSLayoutRelationEqual toItem: self attribute: NSLayoutAttributeLeft multiplier: 1.0 constant: 0];
    NSLayoutConstraint * _Nonnull top = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeTopMargin relatedBy: NSLayoutRelationEqual toItem: self attribute: NSLayoutAttributeTopMargin multiplier: 1.0 constant: 0];
    NSLayoutConstraint * _Nonnull bottom = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeBottomMargin relatedBy: NSLayoutRelationEqual toItem: self attribute: NSLayoutAttributeBottomMargin multiplier: 1.0 constant: 0];
    NSLayoutConstraint * _Nonnull trailing = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeRight relatedBy: NSLayoutRelationEqual toItem: self attribute: NSLayoutAttributeRight multiplier: 1.0 constant: 0];
    [self addSubview: view];
    [self addConstraints: @[leading, top, trailing, bottom]];
    [self layoutIfNeeded];
    [self onCreateView];
}


/**
 Method which provide to get of the nib name

 @return {@link NSString} value of the nib name
 */
-(NSString *) nibName {
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
    return nil;
}

/**
 Method which provide the action when view was created
 */
-(void) onCreateView {
    
}

@end
