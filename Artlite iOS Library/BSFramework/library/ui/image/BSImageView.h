//
//  BSImageView.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface BSImageView : UIImageView

/**
 Tint color for image
 */
@property (nonatomic, strong) IBInspectable UIColor * imageTintColor;

/**
 Method which provide the setting instance of the {@link UIImage}
 with the instance of the {@link UIColor}

 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 */
-(void) setImage:(UIImage * _Nullable) image
        withTint: (UIColor * _Nullable) color;

/**
 Method which provide the setting instance of the {@link NSString}
 with the instance of the {@link UIColor}
 
 @param image instance of the {@link NSString}
 @param color instance of the {@link UIColor}
 */
-(void) setImageName:(NSString * _Nullable) image
            withTint: (UIColor * _Nullable) color;

@end

NS_ASSUME_NONNULL_END
