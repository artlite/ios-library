//
//  BSImageView.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSImageView.h>

@implementation BSImageView

/**
 Method which provide the setting instance of the {@link UIImage}
 with the instance of the {@link UIColor}
 
 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 */
-(void) setImage:(UIImage * _Nullable) image
        withTint: (UIColor * _Nullable) color {
    if (color == nil) {
        [self setImage: image];
    }
    if (image != nil) {
        UIImage * const newImage = [image imageWithRenderingMode:
                                    UIImageRenderingModeAlwaysTemplate];
        [self setTintColor: color];
        [self setImage: newImage];
    }
}

/**
 Method which provide the setting instance of the {@link NSString}
 with the instance of the {@link UIColor}
 
 @param image instance of the {@link NSString}
 @param color instance of the {@link UIColor}
 */
-(void) setImageName:(NSString * _Nullable) image
            withTint: (UIColor * _Nullable) color {
    UIImage * const imageObject = [UIImage imageNamed: image];
    [self setImage: imageObject withTint: color];
}

/**
 Method which provide the image tint color

 @param imageTintColor instance of the {@link UIColor}
 */
- (void)setImageTintColor:(UIColor *)imageTintColor {
    _imageTintColor = imageTintColor;
    [self setImage:self.image withTint:_imageTintColor];
}

/**
 Method which provide the preparing for the interface builder
 */
-(void)prepareForInterfaceBuilder {
    
}

@end
