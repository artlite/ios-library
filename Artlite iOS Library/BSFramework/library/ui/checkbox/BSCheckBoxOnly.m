//
//  BSCheckBoxOnly.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/18/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSCheckBoxOnly.h>

/**
 Class which provide the check box functionality
 */
@interface BSCheckBoxOnly()
/**
 Instance of the {@link NSLayoutConstraint}
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCheckboxWidth;

/**
 Instance of the {@link NSLayoutConstraint}
 */
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constCheckboxHeight;

/**
 Instance of the {@link UIImageView}
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageCheckbox;

@end

/**
 Class which provide the check box functionality
 */
@implementation BSCheckBoxOnly

/**
 Method which provide to get of the nib name
 
 @return {@link NSString} value of the nib name
 */
-(NSString *) nibName {
    return @"BSCheckBoxOnly";
}

/**
 Method which provide the action when view was created
 */
-(void) onCreateView {
    
}

/**
 Method which provide the avake from nib
 */
-(void)awakeFromNib {
    [super awakeFromNib];
    UIImage * unckecked = self.imageCheckbox.image;
    UIImage * checked = self.imageCheckbox.highlightedImage;
    UIColor * tint = self.imageCheckbox.tintColor;
    if (self.imageUnchecked != nil) {
        unckecked = self.imageUnchecked;
    }
    if (self.imageChecked != nil) {
        checked = self.imageChecked;
    }
    if (self.checkBoxColor != nil) {
        tint = self.checkBoxColor;
    }
    if (self.checkBoxSize >= 0) {
        self.constCheckboxWidth.constant = self.checkBoxSize;
        self.constCheckboxHeight.constant = self.checkBoxSize;
    }
    UIImage * normal = [unckecked imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    UIImage * highlight = [checked imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.imageCheckbox.image = normal;
    self.imageCheckbox.highlightedImage = highlight;
    self.imageCheckbox.tintColor = tint;
    [BSGestureHelper addTap: self withView: self
               withSelector: @selector(onCheckboxClick)];
}

/**
 Method which provide the checkbox click
 */
-(void) onCheckboxClick {
    self.imageCheckbox.highlighted = !self.imageCheckbox.highlighted;
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf != nil) {
            [weakSelf.delegate checkBoxOnly:weakSelf
                                    clicked:weakSelf.imageCheckbox.highlighted];
        }
    });
}

/**
 Method which provide if the check box is checked

 @return if checkbox is checkbox
 */
- (BOOL)isChecked {
    return self.imageCheckbox.highlighted;
}

/**
 Method which provide the setting is checked

 @param isChecked isChecked
 */
- (void)setIsChecked:(BOOL)isChecked {
    self.imageCheckbox.highlighted = isChecked;
}

/**
 Method which provide the preparing for the interface builder
 */
-(void)prepareForInterfaceBuilder {
    
}

@end
