//
//  BSTableView.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSTableView.h>

@interface BSTableView()

/**
 Instance of the {@link UITableView}
 */
@property (weak, nonatomic) IBOutlet UITableView *viewTable;

/**
 Instance of the {@link NSMutableArray}
 */
@property (nonatomic, strong) NSMutableArray<NSObject<BSTableObjectDelegate> *> * objects;

/**
 Instance of the {@link UIView}
 */
@property (weak, nonatomic) IBOutlet UIView *viewEmptyList;

/**
 Instance of the {@link UIImageView}
 */
@property (weak, nonatomic) IBOutlet UIImageView *imageEmptyList;

/**
 Instance of the {@link UILabel}
 */
@property (weak, nonatomic) IBOutlet UILabel *labelEmptyTitle;

/**
 Instance of the {@link UILabel}
 */
@property (weak, nonatomic) IBOutlet UILabel *labelEmptyDescription;

/**
 Instance of the {@link UIRefreshControl}
 */
@property (nonatomic, strong) UIRefreshControl * refreshControl;

/**
 Integer value of the size list old
 */
@property (assign) int listSizeOld;

@end

@implementation BSTableView

/**
 Method which provide to get of the nib name
 
 @return {@link NSString} value of the nib name
 */
-(NSString *)nibName {
    return @"BSTableView";
}

/**
 Method which provide the action when view was created
 */
-(void) onCreateView {
    self.selection = YES;
    self.multiplySelection = NO;
    self.objects = [[NSMutableArray<NSObject<BSTableObjectDelegate> *> alloc] init];
    self.viewTable.dataSource = self;
    self.viewTable.delegate = self;
    self.viewTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.viewTable.rowHeight = UITableViewAutomaticDimension;
    self.viewTable.estimatedRowHeight = 160.0;
    self.listSizeOld = -1;
    // Refresh control
    if (self.needSwipeDownRefresh == YES) {
        self.refreshControl = [[UIRefreshControl alloc] init];
        [self.refreshControl addTarget: self
                                action: @selector(refresh)
                      forControlEvents: UIControlEventValueChanged];
        if (@available(iOS 10.0, *)) {
            self.viewTable.refreshControl = self.refreshControl;
        } else {
            [self.viewTable addSubview:self.refreshControl];
        }
    }
    if (self.needEmptyView == NO) {
        [self hideEmptyView];
    }
}

/**
 Method which provide the avake from nib
 */
-(void)awakeFromNib {
    [super awakeFromNib];
    self.viewTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    if (self.separatorColor != nil) {
        if (self.separatorColor == [UIColor clearColor]) {
            self.viewTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        } else {
            self.viewTable.separatorColor = self.separatorColor;
        }
    }
    if ((self.refreshControl != nil) && (self.refreshColor != nil)) {
        self.refreshControl.tintColor = self.refreshColor;
        if (self.refreshTitle != nil) {
            self.refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:self.refreshTitle attributes:@{NSForegroundColorAttributeName:self.refreshColor}];
        }
    }
    if (self.selection == YES) {
        self.viewTable.allowsSelection = YES;
        if (self.multiplySelection == YES) {
            self.viewTable.allowsMultipleSelection = YES;
        }
    } else {
        self.viewTable.allowsSelection = NO;
        self.viewTable.allowsMultipleSelection = NO;
    }
}

#pragma mark - TableView delegate

/**
 Method which provide the getting of the instance of the {@link UITableViewCell}
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 @return instance of the {@link UITableViewCell}
 */
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView
                 cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    //self.tableView.register(UINib(nibName: "NAME_OF_THE_CELL_CLASS", bundle: nil), forCellReuseIdentifier: "REUSE_IDENTIFIER");
    NSInteger row = indexPath.row;
    NSObject<BSTableObjectDelegate> * _Nullable object = [self getObject: @(row)];
    NSString * _Nonnull identifier = [self createIdentifier: [object tableCellType]];
    UITableViewCell * _Nullable cell = [tableView dequeueReusableCellWithIdentifier: identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
                                      reuseIdentifier: identifier];
        if (object != nil) {
            
            // Get view
            UIView * _Nullable view = [object tableObjectView];
            
            // Create constraints
            NSLayoutConstraint * _Nonnull leading = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeRight relatedBy: NSLayoutRelationEqual toItem: cell.contentView attribute: NSLayoutAttributeRight multiplier: 1.0 constant: 0];
            NSLayoutConstraint * _Nonnull top = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeTop relatedBy: NSLayoutRelationEqual toItem: cell.contentView attribute: NSLayoutAttributeTop multiplier: 1.0 constant: 0];
            NSLayoutConstraint * _Nonnull bottom = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeBottom relatedBy: NSLayoutRelationEqual toItem: cell.contentView attribute: NSLayoutAttributeBottom multiplier: 1.0 constant: 0];
            NSLayoutConstraint * _Nonnull trailing = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeLeft relatedBy: NSLayoutRelationEqual toItem: cell.contentView attribute: NSLayoutAttributeLeft multiplier: 1.0 constant: 0];
            
            // Add view and constraint
            [cell.contentView addSubview: view];
            [cell.contentView addConstraints: @[leading, top, trailing, bottom]];
            
            // Add view to the views array
            //            [self.views setObject: view forKey: identifier];
        }
    }
    
    if (cell != nil) {
        UIView<BSTableItemViewDelegate> * _Nullable view = [cell.contentView.subviews firstObject];
        if (view != nil) {
            // Configure view and object
            [object tableObjectConfigure: view withCell: cell];
            if ([view.classForCoder conformsToProtocol:@protocol(BSTableItemViewDelegate)]) {
                [view tableItemViewUpdateId: [object tableObjectId]];
            }
            
            // Configure delegates
            [object tableObjectUpdateDelegate: self];
            if ([view.classForCoder conformsToProtocol:@protocol(BSTableItemViewDelegate)]) {
                [view tableItemViewUpdateDelegate: self];
                [view tableItemViewUpdateObject: object];
            }
        }
    }
    
    cell.backgroundColor = self.backgroundColor;
    [cell sizeToFit];
    [self checkForLazyLoad: @(row).intValue];
    return cell;
}

/**
 Method which provide the getting of the height for the cell at index path
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 @return {@link CGFloat} value of the heigh
 */
-(CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

/**
 Method which provide the gettig of the number of rows in section
 
 @param tableView instance of the {@link UITableView}
 @param section {@link NSInteger} value of the section
 @return {@link NSInteger} value of the number of rows
 */
- (NSInteger)tableView:(nonnull UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.objects.count;
    if (count == 0) {
        [self showEmptyView];
    } else {
        [self hideEmptyView];
    }
    return count;
}

/**
 Method which provide the functional when item was clicked
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSObject<BSTableObjectDelegate> * _Nullable object = [self getObject: @(row)];
    UITableViewCell * _Nullable cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell != nil) {
        UIView<BSTableItemViewDelegate> * _Nullable view = [cell.contentView.subviews firstObject];
        if (view != nil) {
            [object tableObjectDidSelectWithTable: self
                                         withView: view
                                        withIndex: indexPath];
        }
    }
}

/**
 Method which provide the deselecting functional
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableView:(UITableView *)tableView
didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSObject<BSTableObjectDelegate> * _Nullable object = [self getObject: @(row)];
    UITableViewCell * _Nullable cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell != nil) {
        UIView<BSTableItemViewDelegate> * _Nullable view = [cell.contentView.subviews firstObject];
        if (view != nil) {
            [object tableObjectDidDeselectWithTable: self
                                           withView: view
                                          withIndex: indexPath];
        }
    }
}

/**
 Method which provide the checking if the cell can be edited
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 @return {@link BOOL} value if this item can be eddited
 */
-(BOOL)tableView:(UITableView *)tableView
canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSObject<BSTableObjectDelegate> * _Nullable object = [self getObject: @(row)];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if ((object != nil) && (cell != nil)) {
        CGFloat height = cell.frame.size.height;
        NSArray<UITableViewRowAction *> * actions = [object tableObjectRowActions: self
                                                                        withIndex: indexPath
                                                                       withHeight: height];
        if (actions != nil) {
            return actions.count > 0;
        }
    }
    return NO;
}

/**
 Method which provide the getting of the actions
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 @return instance of the {@link NSArray}
 */
-(NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView
                 editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = indexPath.row;
    NSObject<BSTableObjectDelegate> * _Nullable object = [self getObject: @(row)];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if ((object != nil) && (cell != nil)) {
        CGFloat height = cell.frame.size.height;
        NSArray<UITableViewRowAction *> * actions = [object tableObjectRowActions: self
                                                                        withIndex: indexPath
                                                                       withHeight: height];
        if (actions != nil) {
            return actions;
        }
    }
    return [[NSArray<UITableViewRowAction *> alloc] init];
}

#pragma mark - Management
#pragma mark - Clear

/**
 Method which provide the clearing of the objects
 */
-(void) clear {
    [self setObjects: nil];
}

#pragma mark - Set

/**
 Method which provide the adding of the instance of the {@link BSBaseTableObject}
 
 @param object instance of the {@link BSBaseTableObject}
 */
- (void) setObject: (NSObject<BSTableObjectDelegate> * _Nullable) object {
    if (object == nil) {
        [self setObjects: nil];
        return;
    }
    [self setObjects: @[object]];
}

/**
 Method which provide the set of the view
 
 @param view instance of the {@link UIView}
 */
- (void) setView: (UIView * _Nullable) view {
    if (view == nil) {
        [self setViews: nil];
        return;
    }
    [self setViews: @[view]];
}

/**
 Method which provide the adding of the instance of the {@link NSArray}
 
 @param objects instance of the {@link NSArray}
 */
- (void) setObjects: (NSArray<NSObject<BSTableObjectDelegate> *> * _Nullable) objects {
    if (objects != nil) {
        _objects = [[NSMutableArray alloc] initWithArray: objects];
    } else {
        _objects = [[NSMutableArray alloc] init];
    }
    self.listSizeOld = @(objects.count - 1).intValue;
    [self update: YES];
}

/**
 Method which provide the set of the view
 
 @param views instance of the {@link NSArray}
 */
- (void) setViews: (NSArray<UIView *>* _Nullable) views {
    if (views == nil) {
        return;
    }
    NSMutableArray<BSTableViewModel *>* viewModels = [[NSMutableArray alloc]init];
    for (UIView * view in views) {
        if (view != nil) {
            BSTableViewModel * model = [[BSTableViewModel alloc]init:view];
            [viewModels addObject:model];
        }
    }
    [self setObjects:viewModels];
}

#pragma mark - Add

/**
 Method which provide the set of the view
 
 @param view instance of the {@link UIView}
 */
- (void) addView: (UIView * _Nullable) view {
    if (view == nil) {
        return;
    }
    [self addViews: @[view]];
}

/**
 Method which provide the adding of the instance of the {@link BSBaseTableObject}
 
 @param object instance of the {@link BSBaseTableObject}
 */
- (void) addObject: (NSObject<BSTableObjectDelegate> * _Nullable) object {
    if (object == nil) {
        return;
    }
    [self addObjects: @[object]];
}

/**
 Method which provide the set of the view
 
 @param views instance of the {@link NSArray}
 */
- (void) addViews: (NSArray<UIView *>* _Nullable) views {
    if (views == nil) {
        return;
    }
    NSMutableArray<BSTableViewModel *>* viewModels = [[NSMutableArray alloc]init];
    for (UIView * view in views) {
        if (view != nil) {
            BSTableViewModel * model = [[BSTableViewModel alloc]init:view];
            [viewModels addObject:model];
        }
    }
    [self addObjects:viewModels];
}

/**
 Method which provide the adding of the instance of the {@link NSArray}
 
 @param objects instance of the {@link NSArray}
 */
- (void) addObjects: (NSArray<NSObject<BSTableObjectDelegate> *> * _Nullable) objects {
    if (self.objects == nil) {
        self.objects = [[NSMutableArray alloc]init];
    }
    if (objects != nil) {
        [self.objects addObjectsFromArray: objects];
        [self update: NO];
    }
}

#pragma mark - Remove

/**
 Method which provide the removing the object by index
 
 @param object instance of the {@link NSObject<BSTableObjectDelegate>}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObject: (NSObject<BSTableObjectDelegate> * _Nullable) object {
    if (object == nil) {
        return NO;
    }
    return [self removeObjects: @[object]];
}

/**
 Method which provide the removing the object by index
 
 @param objects instance of the {@link NSArray}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjects: (NSArray<NSObject<BSTableObjectDelegate> *> * _Nullable) objects {
    if ((objects == nil) || (objects.count == 0)) {
        return  NO;
    }
    NSMutableArray<NSIndexPath *> * paths = [[NSMutableArray<NSIndexPath *> alloc] init];
    for (NSObject<BSTableObjectDelegate> * object in objects) {
        NSUInteger row = [self.objects indexOfObject: object];
        [paths addObject: [NSIndexPath indexPathForRow: row inSection: 0]];
        [self.objects removeObject: object];
    }
    if (paths.count > 0) {
        [self.viewTable deleteRowsAtIndexPaths: paths
                              withRowAnimation: UITableViewRowAnimationFade];
    }
    return YES;
}

/**
 Method which provide the removing the object by index
 
 @param index instance of the {@link NSNumber}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndex: (NSNumber * _Nullable) index {
    if (index == nil) {
        return NO;
    }
    return [self removeObjectAtIndexes: @[index]];
}

/**
 Method which provide the removing the object by index
 
 @param indexes instance of the {@link NSArray}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndexes: (NSArray<NSNumber *>* _Nullable) indexes {
    if ((indexes == nil) || (indexes.count == 0)) {
        return  NO;
    }
    NSMutableArray<NSIndexPath *> * paths = [[NSMutableArray<NSIndexPath *> alloc] init];
    for (NSNumber * index in indexes) {
        if ((self.objects.count > 0) && (index.integerValue >= 0)) {
            if (index.integerValue <= self.objects.count - 1) {
                [self.objects removeObjectAtIndex: index.integerValue];
                [paths addObject: [NSIndexPath indexPathForRow: index.integerValue inSection: 0]];
            }
        }
    }
    if (paths.count > 0) {
        [self.viewTable deleteRowsAtIndexPaths: paths
                              withRowAnimation: UITableViewRowAnimationFade];
    }
    return YES;
}

/**
 Method which provide the removing the object by index path
 
 @param index instance of the {@link NSIndexPath}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndexPath: (NSIndexPath * _Nullable) index {
    if (index == nil) {
        return NO;
    }
    return [self removeObjectAtIndexPaths: @[index]];
}

/**
 Method which provide the removing the object by index path
 
 @param indexes instance of the {@link NSArray}
 @return {@link BOOL} value if it removed
 */
- (BOOL) removeObjectAtIndexPaths: (NSArray<NSIndexPath *>* _Nullable) indexes {
    if ((indexes == nil) || (indexes.count == 0)) {
        return  NO;
    }
    NSMutableArray<NSIndexPath *> * paths = [[NSMutableArray<NSIndexPath *> alloc] init];
    for (NSIndexPath * index in indexes) {
        if ((self.objects.count > 0) && (index.row >= 0)) {
            if (index.row <= self.objects.count - 1) {
                [self.objects removeObjectAtIndex: index.row];
                [paths addObject: index];
            }
        }
    }
    if (paths.count > 0) {
        [self.viewTable deleteRowsAtIndexPaths: paths
                              withRowAnimation: UITableViewRowAnimationFade];
    }
    return YES;
}

#pragma mark - Get

/**
 Method which provide the getting of the instance of the {@link BSBaseTableObject}
 
 @param index instance of the {@link NSInteger}
 @return instance of the {@link BSBaseTableObject}
 */
- (NSObject<BSTableObjectDelegate> * _Nullable) getObject: (NSNumber * _Nullable) index {
    if (index == nil) {
        return nil;
    }
    NSUInteger count = self.objects.count;
    if ((count > 0) && (index.integerValue >= 0)) {
        if (index.integerValue <= count - 1) {
            return self.objects[index.integerValue];
        }
    }
    return nil;
}

/**
 Method which provide the getting objects
 
 @return instance of the {@link NSArray}
 */
- (NSArray<NSObject<BSTableObjectDelegate> *> * _Nonnull) getObjects {
    return [self.objects copy];
}

#pragma mark - Notify

/**
 Method which provide the update information
 */


/**
 Method which provide the update information
 
 @param needReloadViews {@link BOOL} value if need reloading of the views
 */
- (void) update: (BOOL) needReloadViews {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (weakSelf != nil) {
            [weakSelf.viewTable reloadData];
            if (needReloadViews == YES) {
                [weakSelf.viewTable reloadInputViews];
            }
        }
    });
}

#pragma mark - Show/hide empty screen

/**
 Method which provide the show of the empty view
 */
- (void) showEmptyView {
    if (self.needEmptyView == NO) {
        return;
    }
    [self layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration: 0.5 animations:^{
        if (weakSelf != nil) {
            weakSelf.viewEmptyList.alpha = 1.0f;
            [weakSelf layoutIfNeeded];
        }
    }];
}

/**
 Method which provide the show of the empty view
 */
- (void) hideEmptyView {
    [self layoutIfNeeded];
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration: 0.5 animations:^{
        if (weakSelf != nil) {
            weakSelf.viewEmptyList.alpha = 0.0f;
            [weakSelf layoutIfNeeded];
        }
    }];
}

/**
 Method which provide to create the identifier from instance of the {@link NSIndexPath}
 
 @param index instance of the {@link NSIndexPath}
 @return {@link NSString} value of the cell identifier
 */
- (NSString * _Nonnull) createIdentifier: (NSString * _Nonnull) index {
    return [NSString stringWithFormat:@"BSTableView:Cell:%@", index];
}

#pragma mark - BSTableViewInnerDelegate

/**
 Method which provide the sending event for the object ID
 
 @param event {@link NSString} value of event
 @param objectId {@link NSString} value of the table ID
 */
-(void) tableViewInnerDelegateSendEvent: (NSString * _Nullable) event
                           withObjectId: (NSString * _Nullable) objectId {
    if ((objectId == nil) || (self.delegate == nil)) {
        return;
    }
    NSObject<BSTableObjectDelegate> * object = nil;
    for (NSObject<BSTableObjectDelegate> * innerObject in self.objects) {
        if (innerObject != nil) {
            NSString * innerObjectId = [innerObject tableObjectId];
            if ([innerObjectId isEqualToString: objectId]) {
                object = innerObject;
                break;
            }
        }
    }
    if (object != nil) {
        [self.delegate tableView: self
                 didRecieveEvent: event
                      withObject: object];
    }
}

/**
 Method which provide the set of the indicator style
 
 @param style instance of the {@link UIScrollViewIndicatorStyle}
 */
- (void) setIndicatorStyle: (UIScrollViewIndicatorStyle) style {
    self.viewTable.indicatorStyle = style;
}

#pragma mark - Refresh methods

/**
 Method which provide the refresh functionality
 */
- (void) refresh {
    if (self.delegate != nil) {
        [self showRefresh];
        [self.delegate tableViewRefresh: self];
    }
}

/**
 Method which provide the show refresh
 */
- (void) showRefresh {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if ((weakSelf != nil) && (weakSelf.refreshControl != nil)) {
            [weakSelf.refreshControl beginRefreshing];
        }
    });
}

/**
 Method which provide the hide refresh
 */
- (void) hideRefresh {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if ((weakSelf != nil) && (weakSelf.refreshControl != nil)) {
            [weakSelf.refreshControl endRefreshing];
        }
    });
}

#pragma mark - Lazy load

/*
 private func checkForLazyLoad(index: Int!) {
 let listItemSize: Int! = self.objects.count;
 if ((index == listItemSize - 1)
 && (listItemSize > self.listSizeOld)) {
 self.delegate?.onAlmostAtBottom(listSize: listItemSize);
 self.listSizeOld = listItemSize;
 }
 }
 */

/**
 Method which provide the checking for the lazy loading
 
 @param index value
 */
- (void) checkForLazyLoad:(int) index {
    int listItemSize = @(self.objects.count).intValue;
    if ((index == listItemSize - 1) && (listItemSize > self.listSizeOld)) {
        if (self.delegate != nil) {
            [self.delegate tableView: self
                      almostAtBottom: listItemSize];
        }
        self.listSizeOld = listItemSize;
    }
}

#pragma mark - Setters

/**
 Method which provide the need of the scroll indicator

 @param needScrollIndicator value
 */
-(void)setNeedScrollIndicator:(BOOL)needScrollIndicator {
    _needScrollIndicator = needScrollIndicator;
    if (_needScrollIndicator == YES) {
        self.viewTable.showsVerticalScrollIndicator = YES;
    } else {
        self.viewTable.showsVerticalScrollIndicator = NO;
    }
}

/**
 Method which provide the preparing for the interface builder
 */
-(void)prepareForInterfaceBuilder {
    
}

@end
