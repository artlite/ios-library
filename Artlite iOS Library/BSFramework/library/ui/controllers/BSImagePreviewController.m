//
//  BSImagePreviewController.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 7/27/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSImagePreviewController.h>

/**
 Interface
 */
@interface BSImagePreviewController ()<UIScrollViewDelegate>
@property (nonatomic, strong, nonnull) UIImage * image;
@property (weak, nonatomic) IBOutlet UIScrollView *viewScroll;
@property (weak, nonatomic) IBOutlet UIImageView *viewImage;
@property (weak, nonatomic) IBOutlet UIView *viewClose;
@end

/**
 Controller which provide the image preview
 */
@implementation BSImagePreviewController

#pragma mark - Create methods

/**
 Method which provide the action when controller loaded
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.viewClose addTap:self withSelector:@selector(onCloseClicked)];
    if (self.viewImage.image == nil) {
        self.viewImage.image = self.image;
    }
}

/**
 Method which provide the action when the view did appear

 @param animated value
 */
-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self onInitScrollView];
}

/**
 Method which provide to init of the scroll view
 */
- (void) onInitScrollView {
    CGSize scrollableSize = CGSizeMake([BSScreenHelper getScreenWitdh], [BSScreenHelper getScreenHeight]);
    [self.viewScroll setContentSize:scrollableSize];
    self.viewScroll.minimumZoomScale = 1.0;
    self.viewScroll.maximumZoomScale = self.viewImage.image.size.width / self.viewScroll.frame.size.width;
    self.viewScroll.delegate = self;
    UITapGestureRecognizer * tap = [self.viewScroll addTap:self withSelector:@selector(handleDoubleTap:)];
    tap.numberOfTapsRequired = 2;
}

#pragma mark - Static

/**
 Method which provide the show of the image preview
 
 @param image object
 */
+ (void) show: (UIImage * _Nullable) image {
    UIViewController * owner = [UIViewController getVisibleController];
    NSBundle * bundle = [NSBundle bundleWithIdentifier:@"com.artlite.BSFramework"];
    if (owner == nil) return;
    if (bundle == nil) return;
    if (image == nil) return;
    BSImagePreviewController * controller = [[BSImagePreviewController alloc]initWithNibName:@"BSImagePreviewController" bundle:bundle];
    controller.modalPresentationStyle = UIModalPresentationOverFullScreen;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    controller.image = image;
    [owner presentViewController:controller animated:YES completion:nil];
}

#pragma mark - UIScrollViewDelegate

/**
 Method which provide the getting of the view for zooming in scroll view

 @param scrollView object
 @return view object
 */
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    return self.viewImage;
}

#pragma mark - Setters

/**
 Method which provide the image setting

 @param image object
 */
-(void)setImage:(UIImage *)image {
    _image = image;
    self.viewImage.image = _image;
}

#pragma mark - Close functional

/**
 Method which provide the close functonal
 */
- (void) onCloseClicked {
    self.viewClose.userInteractionEnabled = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Double tap

/**
 Method which provide the double tap to zoom

 @param gestureRecognizer object
 */
- (void)handleDoubleTap:(UIGestureRecognizer *)gestureRecognizer {
    if (self.viewScroll.zoomScale > self.viewScroll.minimumZoomScale){
        [self.viewScroll setZoomScale:self.viewScroll.minimumZoomScale animated:YES];
    } else {
        [self.viewScroll setZoomScale:self.viewScroll.maximumZoomScale animated:YES];
    }
}

@end
