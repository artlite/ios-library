//
//  BSDialogController.m
//  BSFramework
//
//  Created by Dmitry Lernatovich on 8/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSDialogController.h>

/**
 Interface for the {@link BSDialogController}
 */
@interface BSDialogController ()
@property (weak, nonatomic) IBOutlet UIStackView *viewStack;
@property (nonatomic, strong, nullable) NSArray<UIView *>* innerViews;
@property (nonatomic) BSDialogControllerType type;
@end

/**
 View controller which provide to show of the dialog
 */
@implementation BSDialogController

/**
 String constants of the close notification
 */
static NSString * const K_CLOSE_NOTIFICATION = @"EF3575A4053241C14CE2A7703E458EEF3575A4053241C14CE2A7703E458EB05EC110AF7FAA8A8440966B8360AD3C7CB05EC110AF7FAA8A8440966B8360AD3C7C";

/**
 String constants of the close notification
 */
static NSString * const K_ON_CLOSE_NOTIFICATION = @"C3223C13A7CF454D4504B15D28BBB2AD2C3223C13A7CF454D4504B15D28BBB2AD24BBF0512C6DEF3BF27C0C9824C578254BBF0512C6DEF3BF27C0C9824C57825";

/**
 Method which provide the functional when the controller was created
 */
- (void)viewDidLoad {
    [super viewDidLoad];
    // Hide keyboard
    [self.view endEditing:YES];
    // Add notifications
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(closeController)
     name:K_CLOSE_NOTIFICATION
     object:nil];
    // Inject views
    [self injectViews:self.innerViews];
    // Init blur
    [self onInitBlurStyle];
    [self.view addTap:self withSelector:@selector(closeController)];
}

/**
 Dealoc method
 */
-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/**
 Method which provide the init blur style
 */
- (void) onInitBlurStyle {
    UIBlurEffect * effect = nil;
    switch (self.type) {
        case BSDialogControllerTypeLight:
            effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
            break;
        case BSDialogControllerTypeDark:
            effect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
            break;
    }
    UIVisualEffectView * view = [[UIVisualEffectView alloc] initWithEffect:effect];
    view.translatesAutoresizingMaskIntoConstraints = NO;
    view.alpha = 0.9;
    NSLayoutConstraint * _Nonnull leading = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeLeft relatedBy: NSLayoutRelationEqual toItem: self.view attribute: NSLayoutAttributeLeft multiplier: 1.0 constant: 0];
    NSLayoutConstraint * _Nonnull top = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeTopMargin relatedBy: NSLayoutRelationEqual toItem: self.view attribute: NSLayoutAttributeTopMargin multiplier: 1.0 constant: 0];
    NSLayoutConstraint * _Nonnull bottom = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeBottomMargin relatedBy: NSLayoutRelationEqual toItem: self.view attribute: NSLayoutAttributeBottomMargin multiplier: 1.0 constant: 0];
    NSLayoutConstraint * _Nonnull trailing = [NSLayoutConstraint constraintWithItem: view  attribute: NSLayoutAttributeRight relatedBy: NSLayoutRelationEqual toItem: self.view attribute: NSLayoutAttributeRight multiplier: 1.0 constant: 0];
    [self.view insertSubview:view atIndex:0];
    [self.view addConstraints: @[leading, top, trailing, bottom]];
}

/**
 Method which provide the show controller as dialog

 @param views array of views
 @param type instance of the {@link BSDialogControllerType}
 */
+ (void) show: (NSArray<UIView *>* _Nullable) views
     withType: (BSDialogControllerType) type {
    UIViewController * controller = [UIViewController getVisibleController];
    if (controller == nil) return;
    NSBundle * bundle = [NSBundle bundleWithIdentifier:@"com.artlite.BSFramework"];
    BSDialogController * dialogController = [[BSDialogController alloc]initWithNibName:@"BSDialogController" bundle:bundle];
    dialogController.modalPresentationStyle = UIModalPresentationOverFullScreen;
    dialogController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    dialogController.innerViews = views;
    dialogController.type = type;
    [controller presentViewController:dialogController animated:YES completion:nil];
}

/**
 Method which provide the inject views
 
 @param views instance of the {@link NSArray}
 */
- (void) injectViews:(NSArray<UIView *> * _Nullable) views {
    if (views == nil) return;
    for (UIView * view in views) {
        [self.viewStack addArrangedSubview: view];
        [[NSNotificationCenter defaultCenter]
         addObserver:view
         selector:@selector(onDialogControllerDismissed)
         name:K_ON_CLOSE_NOTIFICATION
         object:nil];
    }
    [self.viewStack sizeToFit];
    [self.viewStack layoutIfNeeded];
}

/**
 Method which provide the close controller
 */
- (void) closeController {
    __block NSArray<UIView *>* views = self.viewStack.arrangedSubviews;
    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter]
         postNotificationName:K_ON_CLOSE_NOTIFICATION
         object:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            for (UIView * view in views) {
                [[NSNotificationCenter defaultCenter] removeObserver:view];
            }
        });
    }];
}

/**
 Method which provide the close functional
 */
+ (void) close {
    [[NSNotificationCenter defaultCenter]
     postNotificationName:K_CLOSE_NOTIFICATION
     object:nil];
}

@end
