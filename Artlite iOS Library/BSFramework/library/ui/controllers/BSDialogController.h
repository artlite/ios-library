//
//  BSDialogController.h
//  BSFramework
//
//  Created by Dmitry Lernatovich on 8/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/**
 View controller which provide to show of the dialog
 */
@interface BSDialogController : UIViewController

/**
 Method which provide the show controller as dialog
 
 @param views array of views
 @param type instance of the {@link BSDialogControllerType}
 */
+ (void) show: (NSArray<UIView *>* _Nullable) views
     withType: (BSDialogControllerType) type;

/**
 Method which provide the close functional
 */
+ (void) close;

@end

NS_ASSUME_NONNULL_END
