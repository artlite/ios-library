//
//  BSImageHelper.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Class which provide the image functioality helping
 */
@interface BSImageHelper : NSObject

/**
 Method which provide the convert {@link UIImage} to base64 {@link NSString}

 @param image instance of the {@link UIImage}
 @return instance of the {@link NSString}
 */
+ (NSString * _Nullable) toBase64: (UIImage * _Nullable) image;

/**
 Method which provide the convert {@link UIImage} to base64 {@link NSString}

 @param image instance of the {@link UIImage}
 @param width integer value of the width
 @param height integer value of the height
 @return instance of the {@link NSString}
 */
+ (NSString * _Nullable) toBase64: (UIImage * _Nullable) image
                        withWidth: (int) width
                       withHeight: (int) height;

/**
 Method which provide the rezie the image with width and height

 @param image instance of the {@link UIImage}
 @param width integer value of the width
 @param height integer value of the height
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) resize: (UIImage * _Nullable) image
                    withWidth: (int) width
                    andHeight: (int) height;

/**
 Method which provide the tint image with color

 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) tint: (UIImage * _Nullable) image
                  withColor: (UIColor * _Nullable) color;

@end

NS_ASSUME_NONNULL_END
