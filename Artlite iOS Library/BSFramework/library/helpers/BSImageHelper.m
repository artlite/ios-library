//
//  BSImageHelper.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSImageHelper.h>

/**
 Float value of the image quality
 */
static const float K_QUALITY = 0.3f;

/**
 Class which provide the image functioality helping
 */
@implementation BSImageHelper

/**
 Method which provide the convert {@link UIImage} to base64 {@link NSString}
 
 @param image instance of the {@link UIImage}
 @return instance of the {@link NSString}
 */
+ (NSString * _Nullable) toBase64: (UIImage * _Nullable) image {
    if (image == nil) {
        return nil;
    }
    return [UIImageJPEGRepresentation(image, K_QUALITY)
            base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

/**
 Method which provide the convert {@link UIImage} to base64 {@link NSString}
 
 @param image image instance of the {@link UIImage}
 @param width integer value of the width
 @param height integer value of the height
 @return instance of the {@link NSString}
 */
+ (NSString * _Nullable) toBase64: (UIImage * _Nullable) image
                        withWidth: (int) width
                       withHeight: (int) height {
    if (image == nil) {
        return nil;
    }
    UIImage *resizedImage = [self resize: image withWidth:width andHeight:height];
    return [UIImageJPEGRepresentation(resizedImage, K_QUALITY)
            base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

/**
 Method which provide the rezie the image with width and height
 
 @param image instance of the {@link UIImage}
 @param width integer value of the width
 @param height integer value of the height
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) resize: (UIImage * _Nullable) image
                    withWidth: (int) width
                    andHeight: (int) height {
    if (image == nil) {
        return nil;
    }
    CGSize newSize = CGSizeMake(width, height);
    CGFloat widthRatio = newSize.width / image.size.width;
    CGFloat heightRatio = newSize.height / image.size.height;
    if (widthRatio > heightRatio) {
        newSize = CGSizeMake(image.size.width * heightRatio,
                             image.size.height * heightRatio);
    } else {
        newSize = CGSizeMake(image.size.width * widthRatio,
                             image.size.height * widthRatio);
    }
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

/**
 Method which provide the tint image with color
 
 @param image instance of the {@link UIImage}
 @param color instance of the {@link UIColor}
 @return instance of the {@link UIImage}
 */
+(UIImage * _Nullable) tint: (UIImage * _Nullable) image
                  withColor: (UIColor * _Nullable) color {
    // begin a new image context, to draw our colored image onto
    UIGraphicsBeginImageContext(image.size);
    
    // get a reference to that context we created
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // set the fill color
    [color setFill];
    
    // translate/flip the graphics context (for transforming from CG* coords to UI* coords
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // set the blend mode to color burn, and the original image
    CGContextSetBlendMode(context, kCGBlendModeColorBurn);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextDrawImage(context, rect, image.CGImage);
    
    // set a mask that matches the shape of the image, then draw (color burn) a colored rectangle
    CGContextClipToMask(context, rect, image.CGImage);
    CGContextAddRect(context, rect);
    CGContextDrawPath(context,kCGPathFill);
    
    // generate a new UIImage from the graphics context we drew onto
    UIImage *coloredImg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //return the color-burned image
    return coloredImg;
}

@end
