//
//  BSColorHelper.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/28/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSColorHelper.h>

@implementation BSColorHelper

/**
 Method which provide the create color from hex string
 
 @param hex instance of the {@link NSString}
 @return instance of the {@link UIColor}
 */
+ (UIColor * _Nonnull) create: (NSString * _Nullable) hex {
    return [BSColorHelper create: hex withAlpha: 1.0];
}

/**
 Method which provide the create color from hex string
 
 @param hex instance of the {@link NSString}
 @param alpha instance of the {@link CGFloat}
 @return instance of the {@link UIColor}
 */
+ (UIColor * _Nonnull) create: (NSString * _Nullable) hex
                    withAlpha: (CGFloat) alpha {
    if (hex == nil) {
        return UIColor.clearColor;
    }
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hex];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed: ((rgbValue & 0xFF0000) >> 16)/255.0
                           green: ((rgbValue & 0xFF00) >> 8)/255.0
                            blue: (rgbValue & 0xFF)/255.0
                           alpha: alpha];
}

@end
