//
//  ScreenHelper.h
//  EsteeHR
//
//  Created by Dmitry Lernatovich on 10/22/15.
//  Copyright © 2015 Magnet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface BSScreenHelper : NSObject

/**
 *  @author Dmitriy Lernatovich
 *
 *  Method which provide the getting of the screen height
 *
 *  @return current screen height
 */
+ (CGFloat)getScreenHeight;

/**
 *  @author Dmitriy Lernatovich
 *
 *  Method which provide the getting of the screen width
 *
 *  @return current screen width
 */
+ (CGFloat)getScreenWitdh;

@end
