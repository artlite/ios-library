//
//  BSGestureHelper.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSGestureHelper.h>

/**
 Class which provide the gesture functionality
 */
@implementation BSGestureHelper

/**
 Method which provide the add swipe gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param direction instance of the {@link UISwipeGestureRecognizerDirection}
 @param selector instance of the selector
 @return instance of the {@link UISwipeGestureRecognizer}
 */
+(UISwipeGestureRecognizer * _Nullable) addSwipe: (id) owner
                                        withView: (UIView * _Nullable) view
                                   withDirection: (UISwipeGestureRecognizerDirection) direction
                                    withSelector: (SEL) selector {
    if (view == nil) {
        return nil;
    }
    UISwipeGestureRecognizer * gesture = [[UISwipeGestureRecognizer alloc]
                                          initWithTarget: owner action: selector];
    gesture.direction = direction;
    [view addGestureRecognizer: gesture];
    return gesture;
}

/**
 Method which provide the add swipe gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param direction instance of the {@link UIRectEdge}
 @param selector instance of the selector
 @return instance of the {@link UIScreenEdgePanGestureRecognizer}
 */
+(UIScreenEdgePanGestureRecognizer * _Nullable) addEdgeSwipe: (id) owner
                                                    withView: (UIView * _Nullable) view
                                                    withEdge: (UIRectEdge) direction
                                                withSelector: (SEL) selector {
    if (view == nil) {
        return nil;
    }
    UIScreenEdgePanGestureRecognizer * pan = [[UIScreenEdgePanGestureRecognizer alloc]
                                                  initWithTarget: owner action: selector];
    pan.edges = direction;
    [view addGestureRecognizer: pan];
    return pan;
}

/**
 Method which provide the add tap gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param selector instance of the selector
 @return instance of the {@link UIScreenEdgePanGestureRecognizer}
 */
+(UITapGestureRecognizer * _Nullable) addTap: (id) owner
                                    withView: (UIView * _Nullable) view
                                withSelector: (SEL) selector {
    if (view == nil) {
        return nil;
    }
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc]
                                    initWithTarget: owner action: selector];
    [view addGestureRecognizer: tap];
    return tap;
}

/**
 Method which provide the add long tap gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param selector instance of the selector
 @return instance of the {@link UILongPressGestureRecognizer}
 */
+(UILongPressGestureRecognizer * _Nullable) addLongTap: (id) owner
                                              withView: (UIView * _Nullable) view
                                          withSelector: (SEL) selector {
    if (view == nil) {
        return nil;
    }
    UILongPressGestureRecognizer * tap = [[UILongPressGestureRecognizer alloc]
                                             initWithTarget: owner action: selector];
    [view addGestureRecognizer: tap];
    return tap;
}

@end
