//
//  BSGestureHelper.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Class which provide the gesture functionality
 */
@interface BSGestureHelper : NSObject

/**
 Method which provide the add swipe gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param direction instance of the {@link UISwipeGestureRecognizerDirection}
 @param selector instance of the selector
 @return instance of the {@link UISwipeGestureRecognizer}
 */
+(UISwipeGestureRecognizer * _Nullable) addSwipe: (id) owner
                                        withView: (UIView * _Nullable) view
                                   withDirection: (UISwipeGestureRecognizerDirection) direction
                                    withSelector: (SEL) selector;

/**
 Method which provide the add edge swipe gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param direction instance of the {@link UIRectEdge}
 @param selector instance of the selector
 @return instance of the {@link UIScreenEdgePanGestureRecognizer}
 */
+(UIScreenEdgePanGestureRecognizer * _Nullable) addEdgeSwipe: (id) owner
                                                    withView: (UIView * _Nullable) view
                                                    withEdge: (UIRectEdge) direction
                                                withSelector: (SEL) selector;

/**
 Method which provide the add tap gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param selector instance of the selector
 @return instance of the {@link UITapGestureRecognizer}
 */
+(UITapGestureRecognizer * _Nullable) addTap: (id) owner
                                    withView: (UIView * _Nullable) view
                                withSelector: (SEL) selector;

/**
 Method which provide the add long tap gesture
 
 @param owner instance of the owner
 @param view instance of the {@link UIView}
 @param selector instance of the selector
 @return instance of the {@link UILongPressGestureRecognizer}
 */
+(UILongPressGestureRecognizer * _Nullable) addLongTap: (id) owner
                                              withView: (UIView * _Nullable) view
                                          withSelector: (SEL) selector;

@end

NS_ASSUME_NONNULL_END
