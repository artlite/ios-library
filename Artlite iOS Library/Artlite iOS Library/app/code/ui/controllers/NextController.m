//
//  NextController.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "NextController.h"
#import "DialogTestView.h"
#import <BSFramework/BSFramework.h>

@interface NextController ()

@end

@implementation NextController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)onTestPressed:(id)sender {
    [[[DialogTestView alloc]initWithFrame:CGRectZero] showAsDialog:BSDialogControllerTypeDark];
}

@end
