//
//  ViewController.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BSFramework/BSFramework.h>

@interface ViewController : UIViewController<BSTableViewDelegate>

@end

