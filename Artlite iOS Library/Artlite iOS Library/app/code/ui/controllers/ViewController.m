//
//  ViewController.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "ViewController.h"
#import "TestTableObject.h"
#import "ButtonsView.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet BSTableView *viewTable;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewTable.delegate = self;
    [self.viewTable setIndicatorStyle: UIScrollViewIndicatorStyleWhite];
}

/**
 Method which provide the action when the event was recieved
 
 @param tableView instance of the {@link BSTableView}
 @param event {@link NSString} value of the event
 @param itemView instance of the {@link UIView}
 @param object instance of the {@link NSObject}
 */
-(void) tableView: (id) tableView
  didRecieveEvent: (NSString * _Nonnull) event
       withObject: (NSObject * _Nonnull) object {
    [BSLogger info: self withMethod: @"didRecieveEvent" withObject: event];
    BSTableView * viewTable = tableView;
    NSObject<BSTableObjectDelegate> * objectToRemove = object;
    [viewTable removeObject: objectToRemove];
}

/**
 Method which provide the table view refreshing

 @param tableView instance of the table vew
 */
-(void)tableViewRefresh:(id)tableView {
    [self set];
    [self.viewTable hideRefresh];
}

/**
 Method which provide the refresh functional when almost at bottom
 
 @param tableView instance of the {@link BSTableView}
 @param size list value
 */
-(void)tableView:(id)tableView
  almostAtBottom:(int)size {
    NSLog(@"Almost at bottom %d", size);
    [self add];
}

/**
 Method which provide the update functionality
 */
- (void) set {
    NSArray<TestTableObject *> * array = @[
                                           [[TestTableObject alloc] initWith: @"Title 1" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 2" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 3" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 4" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 5" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 6" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 7" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 8" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 9" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 10" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 11" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 12" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 13" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 14" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 15" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           ];
    [self.viewTable addViews:@[[[ButtonsView alloc]init], [[ButtonsView alloc]init], [[ButtonsView alloc]init], [[ButtonsView alloc]init]]];
    [self.viewTable addObjects: array];
}

/**
 Method which provide the update functionality
 */
- (void) add {
    NSArray<TestTableObject *> * array = @[
                                           [[TestTableObject alloc] initWith: @"Title 1" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 2" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 3" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 4" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 5" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 6" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 7" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 8" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 9" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 10" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 11" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 12" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           [[TestTableObject alloc] initWith: @"Title 13" andDesc: @"Graphic designers aren't programmers and sometimes don't know how to properly prepare graphic assets for developers. This simple cheatsheet should help them to do their job better, and to simplify developers' lives."],
                                           [[TestTableObject alloc] initWith: @"Title 14" andDesc: @"Notice: the first icon dimension in table cell is full asset size, the second icon dimension is optical square. Dimension values are in pixels."],
                                           [[TestTableObject alloc] initWith: @"Title 15" andDesc: @"Например, обновлённая версия Lenovo IdeaPad S540 теперь предлагается в конфигурациях с процессорами Intel Core i7 8-го поколения или AMD Ryzen 7 3700U. В качестве опции может предлагаться дискретная видеокарта NVIDIA GeForce GTX 1050. Рамки вокруг дисплея стали на 25% уже, а для веб-камеры добавлена функция защиты приватности. Ноутбук Lenovo IdeaPad S540 может оснащаться 14- или 15-дюймовым дисплеем с разрешением Full HD 1920х1080 точек, оперативной памятью объёмом до 12 ГБ, твердотельным накопителем ёмкостью до 512 ГБ. Стоимость 14-дюймовой версии с процессором Intel начинается с отметки $879,99, 15-дюймовой версии с чипом Intel – с $849,99, 14-дюймовой версии с процессором AMD – с $729,99. Продажи новинок начнутся в апреле и июне."],
                                           ];
    [self.viewTable addObjects: array];
}

@end
