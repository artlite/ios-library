//
//  NextController.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2019-04-02.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NextController : UIViewController

@end

NS_ASSUME_NONNULL_END
