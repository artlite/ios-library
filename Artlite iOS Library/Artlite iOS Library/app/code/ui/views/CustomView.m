//
//  CustomView.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "CustomView.h"

@interface CustomView()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation CustomView

static NSString * const K_URL = @"https://cdn.gamerant.com/wp-content/uploads/resident-evil-2-screenshots-ada-wong-header.jpg.optimal.jpg";

-(NSString *)nibName {
    return NSStringFromClass(self.classForCoder);
}

-(void)onCreateView {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        UIImage * image = [BSImageManager download: K_URL];
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            self.imageView.image = image;
        });
    });
}

@end
