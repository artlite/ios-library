//
//  TestTableItemView.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/20/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestTableItemView : BSView<BSTableItemViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelDescription;
@property (weak, nonatomic) IBOutlet UIView *viewBackground;
@end

NS_ASSUME_NONNULL_END
