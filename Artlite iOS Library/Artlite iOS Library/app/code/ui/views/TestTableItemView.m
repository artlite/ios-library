//
//  TestTableItemView.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/20/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "TestTableItemView.h"

@interface TestTableItemView()
@property (nonnull, strong) NSString * objectId;
@property (nonatomic, weak) id<BSTableViewInnerDelegate> delegate;
@end

@implementation TestTableItemView

-(NSString *)nibName {
    return NSStringFromClass(self.classForCoder);
}



/**
 Method which provide the set object ID from table object
 (YOU NEED TO SAVE OBJECT ID SOMEWERE BECAUSE IT USING FOR REUSE CELLS IN THE TABLE VIEW)
 
 @param objectId {@link NSString} value of the object ID
 */
-(void) tableItemViewUpdateId: (NSString * _Nonnull) objectId {
    self.objectId = objectId;
}

/**
 Method which provide the update table item view with delegate
 
 @param delegate instance of the {@link BSTableViewInnerDelegate}
 */
-(void) tableItemViewUpdateDelegate: (id<BSTableViewInnerDelegate>) delegate {
    self.delegate = delegate;
}

@end
