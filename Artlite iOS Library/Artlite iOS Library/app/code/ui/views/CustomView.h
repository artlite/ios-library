//
//  CustomView.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/11/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomView : BSView

@end

NS_ASSUME_NONNULL_END
