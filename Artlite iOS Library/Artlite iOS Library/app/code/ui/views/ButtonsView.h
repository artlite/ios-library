//
//  ButtonsView.h
//  Artlite iOS Library
//
//  Created by Dmitry Lernatovich on 7/7/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BSFramework/BSFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface ButtonsView : BSView

@end

NS_ASSUME_NONNULL_END
