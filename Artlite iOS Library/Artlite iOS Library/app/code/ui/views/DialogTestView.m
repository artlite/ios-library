//
//  DialogTestView.m
//  Artlite iOS Library
//
//  Created by Dmitry Lernatovich on 8/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "DialogTestView.h"

@implementation DialogTestView

/**
 Method which provide to get of the nib name
 
 @return {@link NSString} value of the nib name
 */
-(NSString *) nibName {
    return NSStringFromClass(self.classForCoder);
}

/**
 Method which provide the action when view was created
 */
-(void) onCreateView {
    
}

- (IBAction)onCloseClick:(id)sender {
    [self closeDialog];
}

- (void)onDialogControllerDismissed {
    [self showAsDialog:BSDialogControllerTypeLight];
}

@end
