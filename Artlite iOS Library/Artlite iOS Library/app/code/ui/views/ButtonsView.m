//
//  ButtonsView.m
//  Artlite iOS Library
//
//  Created by Dmitry Lernatovich on 7/7/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "ButtonsView.h"

@implementation ButtonsView

/**
 Method which provide to get of the nib name
 
 @return {@link NSString} value of the nib name
 */
-(NSString *) nibName {
    return NSStringFromClass(self.classForCoder);
}

/**
 Method which provide the action when view was created
 */
-(void) onCreateView {
    
}

@end
