//
//  DialogTestView.h
//  Artlite iOS Library
//
//  Created by Dmitry Lernatovich on 8/12/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface DialogTestView : BSView

@end

NS_ASSUME_NONNULL_END
