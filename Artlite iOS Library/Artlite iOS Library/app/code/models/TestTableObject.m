//
//  TestTableObject.m
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/20/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import "TestTableObject.h"
#import "TestTableItemView.h"

@interface TestTableObject()
@property (nonatomic, strong) NSString * _Nullable title;
@property (nonatomic, strong) NSString * _Nullable desc;
@property (nonatomic, strong) NSString * objectId;
@property (nonatomic, weak) id<BSTableViewInnerDelegate> delegate;
@property (nonatomic, assign) BOOL isSelected;
@end

@implementation TestTableObject

/**
 Mehod which provide the create the {@link TestTableObject} with parameters
 
 @param title instance of the {@link NSString}
 @param desc instance of the {@link NSString}
 @return instance of the {@link TestTableObject}
 */
- (instancetype)initWith: (NSString * _Nullable) title
                 andDesc: (NSString * _Nullable) desc
{
    self = [super init];
    if (self) {
        self.title = title;
        self.desc = desc;
        self.isSelected = NO;
    }
    return self;
}

/**
 Method which provide the getting of the table object id
 (YOU NEED TO SAVE OBJECT ID SOMEWERE BECAUSE IT USING FOR REUSE CELLS IN THE TABLE VIEW)
 
 @return instance of the {@link NSString}
 */
- (NSString * _Nonnull) tableObjectId {
    if (self.objectId == nil) {
        self.objectId = [[NSUUID UUID] UUIDString];
    }
    return self.objectId;
}

/**
 Method which provide the getting of the table cell type
 (YOU NEED TO SAVE OBJECT ID SOMEWERE BECAUSE IT USING FOR REUSE CELLS IN THE TABLE VIEW)
 
 @return instance of the {@link NSString}
 */
- (NSString * _Nonnull) tableCellType {
    return @"TEST_TABLE_CELL";
}

/**
 Method which provide the getting of the instance of the {@link UIView}
 
 @return instance of the {@link UIView}
 */
- (UIView *)tableObjectView {
    return [[TestTableItemView alloc] init];
}

/**
 Method which provide the configure view with object
 
 @param view instance of the {@link UIView}
 @param cell instance of the {@link UITableViewCell}
 */
-(void)tableObjectConfigure:(UIView *)view
                   withCell:(UITableViewCell *)cell {
    if ([view isKindOfClass: [TestTableItemView class]] == YES) {
        TestTableItemView * _Nonnull itemView = (TestTableItemView *) view;
        itemView.labelTitle.text = self.title;
        itemView.labelDescription.text = self.desc;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
}

/**
 Method which provide the functional when item was selected
 
 @param tableView instance of the {@link UITableView}
 @param view instance of the {@link UIView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableObjectDidSelectWithTable:(id)tableView
                            withView: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                           withIndex:(NSIndexPath *)indexPath {
    
    if ([view isKindOfClass: [TestTableItemView class]] == YES) {
        TestTableItemView * _Nonnull itemView = (TestTableItemView *) view;
        itemView.labelTitle.text = [NSString stringWithFormat:@"%@ (selected)", self.title];
    }
}

/**
 Method which provide the functional when item was deselected
 
 @param tableView instance of the {@link UITableView}
 @param view instance of the {@link UIView}
 @param indexPath instance of the {@link NSIndexPath}
 */
-(void)tableObjectDidDeselectWithTable:(id)tableView
                              withView: (UIView<BSTableItemViewDelegate> * _Nonnull) view
                             withIndex:(NSIndexPath *)indexPath {
    if ([view isKindOfClass: [TestTableItemView class]] == YES) {
        TestTableItemView * _Nonnull itemView = (TestTableItemView *) view;
        itemView.labelTitle.text = [NSString stringWithFormat:@"%@", self.title];
    }
}

/**
 Method which provide the getting of the row actions
 
 @param tableView instance of the {@link UITableView}
 @param indexPath instance of the {@link NSIndexPath}
 @param height instance of the {@link CGFloat}
 @return instance of the {@link NSArray}
 */
-(NSArray<UITableViewRowAction *> * _Nullable) tableObjectRowActions: (id) tableView
                                                           withIndex:(NSIndexPath *)indexPath
                                                          withHeight: (CGFloat) height {
    UITableViewRowAction * updateAction = [UITableViewRowAction actionFromImage:[UIImage imageNamed:@"ic_update"] withColor:[BSColorHelper create: @"#FF9800"] withHeight:height withHandler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
            if (self.delegate != nil) {
                [self.delegate tableViewInnerDelegateSendEvent: @"Update action"
                                                  withObjectId: [self tableObjectId]];
            }
        }];
//    [UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDefault title: @"Update" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//            if (self.delegate != nil) {
//                [self.delegate tableViewInnerDelegateSendEvent: @"Update action"
//                                                  withObjectId: [self tableObjectId]];
//            }
//        }];
    UITableViewRowAction * deleteAction = [UITableViewRowAction rowActionWithStyle: UITableViewRowActionStyleDefault title: @"Delete" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        if (self.delegate != nil) {
            [self.delegate tableViewInnerDelegateSendEvent: @"Delete action"
                                              withObjectId: [self tableObjectId]];
        }
    }];
//    updateAction.backgroundColor = [BSColorHelper create: @"#FF9800"];
    deleteAction.backgroundColor = [BSColorHelper create: @"#f44336"];
    return @[deleteAction, updateAction];
}

/**
 Method which provide the update table item view with delegate
 
 @param delegate instance of the {@link BSTableViewInnerDelegate}
 */
-(void)tableObjectUpdateDelegate:(id<BSTableViewInnerDelegate>)delegate {
    self.delegate = delegate;
}

/**
 Method which provide the update object inside the view
 (YOU CAN KEEP IT AS WEAK REFERENCE)
 
 @param object instance of the {@link NSObject}
 */
-(void) tableItemViewUpdateObject: (NSObject * _Nonnull) object {
    
}

@end
