//
//  TestTableObject.h
//  Artlite iOS Library
//
//  Created by dlernatovich on 2/20/19.
//  Copyright © 2019 dlernatovich. All rights reserved.
//

#import <BSFramework/BSFramework.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestTableObject : NSObject <BSTableObjectDelegate>

/**
 Mehod which provide the create the {@link TestTableObject} with parameters
 
 @param title instance of the {@link NSString}
 @param desc instance of the {@link NSString}
 @return instance of the {@link TestTableObject}
 */
- (instancetype)initWith: (NSString * _Nullable) title
                 andDesc: (NSString * _Nullable) desc;

@end

NS_ASSUME_NONNULL_END
